# APIs

## Pets

- **Method**: `POST`, `GET`, `GET`, `PUT`, `DELETE`,
- **Path**: `/api/pets/`, `/api/pets/<str:pet_id>/`

Input:

```json
{
  "name": "string",
  "type": "string",
  "sex": "string",
  "breed": "string",
  "age": 0,
  "color": "string",
  "image_url": "string",
  "fixed": true,
  "good_with_children": true,
  "good_with_animals": true,
  "description": "string",
  "shelter_id": "string"
}
```

Output:

```json
{
  "id": "string",
  "name": "string",
  "type": "string",
  "sex": "string",
  "breed": "string",
  "age": 0,
  "color": "string",
  "image_url": "string",
  "fixed": true,
  "good_with_children": true,
  "good_with_animals": true,
  "description": "string",
  "shelter": {
    "id": "string",
    "shelter_name": "string",
    "address": "string",
    "phone_num": "string",
    "description": "string",
    "account": {
      "id": "string",
      "username": "string",
      "email": "string",
      "full_name": "string",
      "role": "string"
    }
  }
}
```

Creating a new pet saves the name, type, sex, breed, age, color, image_url, fixed, good_with_children, good_with_animals, description, and finds the associated shelter and saves it. This adds a new Pet to the pets database which an adopter user can view and apply to addopt.

- **Additional Methods**: `GET`, `GET`,
- **Path**: `/api/pets/home/featured/`, `/api/pets/shelter/<str:username>/`


## Accounts

- **Method**: `POST`, `GET`, `GET`, `DELETE`,
- **Path**: `/api/accounts/`, `/api/accounts/<str:username>/`

Input:

```json
{
  "username": "string",
  "email": "string",
  "password": "string",
  "full_name": "string",
  "role": "string"
}
```

Output:

```json
{
  "id": "string",
  "username": "string",
  "email": "string",
  "full_name": "string",
  "role": "string"
}
```

Creating a new account saves the username, email, full_name, role, and hashes the password and saves it as hashed_password. This adds a new Account to the accounts database which the user will use to log in to their account. The account then gets a token from the authenticator and saves it.


## Shelters

- **Method**: `POST`, `GET`, `GET`,
- **Path**: `/api/shelters/`, `/api/shelters/<str:username>/`

Input:

```json
{
  "username": "string",
  "shelter_name": "string",
  "address": "string",
  "phone_num": "string",
  "description": "string"
}
```

Output:

```json
{
  "id": "string",
  "shelter_name": "string",
  "address": "string",
  "phone_num": "string",
  "description": "string",
  "account": {
    "id": "string",
    "username": "string",
    "email": "string",
    "full_name": "string",
    "role": "string"
  }
}
```

Creating a new shelter saves the shelter_name, address, phone_num, description, and finds the account associated with the inputted username and saves it. This adds a new Shelter to the shelters database which can then be used by a shelter user to add pets to the database.


## Adopters

- **Method**: `POST`, `GET`,
- **Path**: `/api/adopters/`,

Input:

```json
{
  "username": "string",
  "household_type": "string",
  "household_size": "string",
  "num_of_children": 0,
  "pet_allergies": "string",
  "other_pets": "string",
  "vet_name": "string",
  "vet_address": "string"
}
```

Output:

```json
{
  "id": "string",
  "household_type": "string",
  "household_size": "string",
  "num_of_children": 0,
  "pet_allergies": "string",
  "other_pets": "string",
  "vet_name": "string",
  "vet_address": "string",
  "account": {
    "id": "string",
    "username": "string",
    "email": "string",
    "full_name": "string",
    "role": "string"
  }
}
```

Creating a new adopter saves the household_type, household_size, num_of_children, pet_allergies, other_pets, vet_name, vet_address, and finds the account associated with the inputted username and saves it. This adds a new Adopter to the adopters database which can then be used by an adopter when filling out an adoption application.


## Applications

- **Method**: `POST`, `GET`, `GET`, `PUT`,
- **Path**: `/api/applications/`, `/api/applications/<str:app_id>/`

POST Input:

```json
{
  "username": "string",
  "pet_id": "string",
  "status": "Pending",
  "date_submitted": "string",
  "additional_information": "string"
}
```

PUT Input:

```json
{
  "status": "string"
}
```

Output:

```json
{
  "id": "string",
  "status": "string",
  "date_submitted": "string",
  "additional_information": "string",
  "adopter": {
    "id": "string",
    "household_type": "string",
    "household_size": "string",
    "num_of_children": 0,
    "pet_allergies": "string",
    "other_pets": "string",
    "vet_name": "string",
    "vet_address": "string",
    "account": {
      "id": "string",
      "username": "string",
      "email": "string",
      "full_name": "string",
      "role": "string"
    }
  },
  "pet": {
    "id": "string",
    "name": "string",
    "type": "string",
    "sex": "string",
    "breed": "string",
    "age": 0,
    "color": "string",
    "image_url": "string",
    "fixed": true,
    "good_with_children": true,
    "good_with_animals": true,
    "description": "string",
    "shelter": {
      "id": "string",
      "shelter_name": "string",
      "address": "string",
      "phone_num": "string",
      "description": "string",
      "account": {
        "id": "string",
        "username": "string",
        "email": "string",
        "full_name": "string",
        "role": "string"
      }
    }
  }
}
```

Creating a new application sets the status as 'pending', saves the date_submitted and additional_information, finds the associated account from the inputted username, finds the associated pet from the inputted pet_id and saves them. The Application is then added to the applications database which can then be viewed by the associated adopter user, and viewed, accepted, or rejected by the associated shelter user.

- **Additional Methods**: `GET`, `GET`,
- **Path**: `/api/applications/shelter/<str:username>/`, `/api/applications/adopter/<str:username>/`
