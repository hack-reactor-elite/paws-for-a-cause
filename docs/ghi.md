# Graphical Human Interface

## Home Page

This is our splash page visible to users regardless if they are logged in or not. The featured pets are taken from the database and displays the 4 pets that have been listed on the site the longest.

![Home Page](wireframes/splash.png)

## Login Page
This is a simple log-in page for users, where they can log in if they are an existing user, or they can choose to sign up for an account.

![Login Page](wireframes/login.png)

## Sign up Choice Page
When users choose the "signup" option on the login page, it routes them to this page, where they can choose whether they want to sign up as a shelter user or as an adopter user.

![Sign up Choice Page](wireframes/signup_choice.png)

## Sign up as an Adopter
When a user decides they want to sign up as an adopter, they are routed to the adopter sign up form, where they input their general information as well as information necessary to adopting a pet. This information will be autofilled in the applicaiton review everytime the user submits an application.

![Adopter Signup](wireframes/adopter_signup.png)

## Sign up as a Shelter
When a user decides to sign up as a shelter, they are routed to the shelter sign up form, where they input their general information as well was information about the shelter.

![Shelter Signup](wireframes/shelter_signup.png)

## Universal Views for Logged in Users:
These are pages that are visible to both adopters and shelters, although they operate a little differently depending on what kind of user you are.

## Pet List Page
The pet list page is where adopters and shelters can come to view adoptable pets on the website. For adopters, they are shown every adoptable animal in the database with a link to view their detail pages. For shelters, they are shown every animal that they have listed on the webiste with a button to remove them from the website if desired.

![Pet List](wireframes/pet_list.png)

## Application List Page
The application list page is where adopters and shelters can come to view submitted applications. For adopters, after submitting an application to adopt a pet, they can come to this page to view the status of that application. For shelters, they can visit this page to view any applications sent in to their shelter along with an additional "review" button to take them to the review application page.

![Application List](wireframes/application_list.png)

## Adopter Specific Views:
These are views that are only visible if you are a logged in adopter user.

## Pet Detail Page
On the pet detail page, potential adopters can view more information about a pet they're interested in along with information about the shelter they're at. They can also click "apply to adopt" to take them to the application form.

![Pet Detail](wireframes/pet_detail.png)

## Application Form Page
The application form page is where adopters can submit any additional information about themselves or the pet their interested in for the shelter to consider before submitting an adoption application.

![Application Form](wireframes/application_form.png)

## Shelter Specific Views:
These are views that are only visible if you are a logged in shelter user.

## List Pet Page
The list pet form is where shelters can list pets at their shelter up for adoption on the website. The pet is added to the database and then available for adopters to see on their end.

![List Pet](wireframes/list_pet.png)

## Application Review Page
The application review page is where shelters are routed to after clicking the "review" button on the application list page. Here, they can review comprehensive information about the prospective adopter and decide whether or not to accept or deny the adopters application to adopt.

![Application Review](wireframes/application_review.png)
