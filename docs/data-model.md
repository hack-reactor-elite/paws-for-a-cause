# Data models

---

### Account Model

| name      | type   | unique | optional |
| --------- | ------ | ------ | -------- |
| username  | string | yes    | no       |
| email     | string | no     | no       |
| password  | string | no     | no       |
| full_name | string | no     | no       |
| role      | string | no     | no       |

The `Account` entity contains the data about an account
that a user can create. Users can take on a role of either an Adopter or a Shelter.

---

### Types of Accounts

### Adopters Model

| name            | type   | unique | optional |
| --------------- | ------ | ------ | -------- |
| username        | string | yes    | no       |
| household_type  | string | no     | no       |
| household_size  | string | no     | no       |
| num_of_children | int    | no     | no       |
| pet_allergies   | string | no     | no       |
| other_pets      | string | no     | no       |
| vet_name        | string | no     | no       |
| vet_address     | string | no     | no       |

### Shelters Model

| name         | type   | unique | optional |
| ------------ | ------ | ------ | -------- |
| username     | string | yes    | no       |
| shelter_name | string | no     | no       |
| address      | string | yes    | no       |
| phone_num    | string | no     | no       |
| description  | string | no     | no       |

### Pet Model

| name               | type   | unique | optional |
| ------------------ | ------ | ------ | -------- |
| name               | string | no     | no       |
| type               | string | no     | no       |
| sex                | string | no     | no       |
| breed              | string | no     | no       |
| age                | int    | no     | no       |
| color              | string | no     | no       |
| image_url          | string | no     | no       |
| fixed              | bool   | no     | no       |
| good_with_children | bool   | no     | no       |
| good_with_animals  | bool   | no     | no       |
| description        | string | no     | no       |
| shelter_id         | string | yes    | no       |

### Application Model

| name                   | type   | unique | optional |
| ---------------------- | ------ | ------ | -------- |
| username               | string | yes    | no       |
| pet_id                 | string | yes    | no       |
| status                 | string | no     | no       |
| date_submitted         | string | no     | no       |
| additional_information | string | no     | no       |
