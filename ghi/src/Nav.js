import { NavLink } from 'react-router-dom';
import "./css/nav.css"
import useToken from '@galvanize-inc/jwtdown-for-react';
import { useNavigate } from 'react-router-dom';
import useFetch from "react-fetch-hook"

function Nav() {
    const {token} = useToken();
    const { logout } = useToken();
    const navigate = useNavigate();
    const username = localStorage.getItem("username")
    const {data} = useFetch(`${process.env.REACT_APP_API_HOST}/api/accounts/${username}`)
    let role = "";
    let shelter = false;

    const handleLogout = () => {
        localStorage.setItem("username", "")
        logout();
        navigate("/")
        alert("You have been logged out!");
    };

    if (data){
        role = data.role
    }

    if (role === "shelter") {
        shelter = true;
    }

    return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
        <div className="container-fluid">
            <NavLink className="navbar-brand" to="/">Paws for a Cause</NavLink>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
            {token ? (
                <div>
                        {shelter ? (
                            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                                <span className="navbar-text">Hello, {username}!</span>
                                <li className ="nav-item">
                                    <NavLink className="nav-link" to="/shelter/pets">Available Pets</NavLink>
                                </li>
                                <li className ="nav-item">
                                    <NavLink className="nav-link" to="/shelter/pets/new">List Pet for Adoption</NavLink>
                                </li>
                                <li className ="nav-item">
                                    <NavLink className="nav-link" to="/shelter/applications">Review Applications</NavLink>
                                </li>
                                <li className ="nav-item">
                                    <NavLink className="nav-link" to="/" onClick={handleLogout}>Logout</NavLink>
                                </li>
                            </ul>

                        ):(
                            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                                <span className="navbar-text">Hello, {username}!</span>
                                <li className ="nav-item">
                                    <NavLink className="nav-link" to="/adopter/pets">Available Pets</NavLink>
                                </li>
                                <li className ="nav-item">
                                    <NavLink className="nav-link" to="/adopter/applications">Submitted Applications</NavLink>
                                </li>
                                <li className ="nav-item">
                                    <NavLink className="nav-link" to="/" onClick={handleLogout}>Logout</NavLink>
                                </li>

                            </ul>
                        )}
                    </div>
                    ): (
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                    <div>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/login">Login/Signup!</NavLink>
                    </li>
                    </div>
                    </ul>
            )}
            </div>
        </div>
    </nav>
    )
}

export default Nav;
