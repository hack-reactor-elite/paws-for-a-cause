import HomeCarousel from "./components/HomeCarousel";
import "./css/MainPage.css";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import useFetch from "react-fetch-hook";

function MainPage() {
    let username = localStorage.getItem("username");
    const [pets, setPets] = useState([]);
    const { data } = useFetch(
        `${process.env.REACT_APP_API_HOST}/api/accounts/${username}`
    );
    const [applications, setApplications] = useState([]);
    let role = "";
    let shelter = false;
    const { token } = useToken();

    const fetchPets = async () => {
        try {
            const petResponse = await fetch(
                `${process.env.REACT_APP_API_HOST}/api/pets/home/featured`
            );
            if (petResponse.ok) {
                const petData = await petResponse.json();
                setPets(petData);

                const applicationResponse = await fetch(
                    `${process.env.REACT_APP_API_HOST}/api/applications`
                );
                if (applicationResponse.ok) {
                    const applicationData = await applicationResponse.json();
                    setApplications(applicationData);
                }
            } else {
                console.error(petResponse);
            }
        } catch (error) {
            console.error(error);
        }
    };

    if (data) {
        role = data.role;
    }

    if (role === "shelter") {
        shelter = true;
    }

    useEffect(() => {
        fetchPets();
    }, []);

    const responsive = {
        superLargeDesktop: {
            // the naming can be any, depends on you.
            breakpoint: { max: 3000, min: 1500 },
            items: 4,
        },
        desktop: {
            breakpoint: { max: 1500, min: 1024 },
            items: 3,
        },
        tablet: {
            breakpoint: { max: 1024, min: 464 },
            items: 2,
        },
        mobile: {
            breakpoint: { max: 464, min: 0 },
            items: 1,
        },
    };

    const getStatusForPet = (pet) => {
        const petApplications = applications.filter((app) => app.pet.id === pet.id);
        if (petApplications.length > 0) {
            const mostRecentApplication = petApplications[petApplications.length - 1];
            if (mostRecentApplication.status.toLowerCase() === "pending") {
                return "Available (application pending)";
            } else {
                return "Available";
            }
        } else {
            return "Available";
        }
    };

    return (
        <div>
            <br></br>
            <HomeCarousel className="carousel" />
            <h1 className="heading">Featured Pets</h1>
            <div className="featured">
                <Carousel responsive={responsive}>
                    {pets.map((pet, index) => (
                        <div className="main-card card" key={index}>
                            <div className="card-body">
                                <h5 className="card-text">{pet.name}</h5>
                                <img
                                    src={pet.image_url}
                                    alt={`${pet.pet_type} pic`}
                                    className="pet-card-image"
                                />
                                <div className="col-md-12">
                                    <p className="card-text">Breed: {pet.breed}</p>
                                    <p className="card-text">Age: {pet.age}</p>
                                    <p className="card-text">Sex: {pet.sex}</p>
                                    <p className="card-text">Status: {getStatusForPet(pet)}</p>
                                </div>
                            </div>
                            {token ? (
                                <div>
                                    {shelter ? (
                                        <div></div>
                                    ) : (
                                        <div className="card-footer">
                                            <Link to={`/adopter/pets/${pet.id}`}>
                                                <button className="btn btn-primary">
                                                    View Details
                                                </button>
                                            </Link>
                                        </div>
                                    )}
                                </div>
                            ) : (
                                <div></div>
                            )}
                        </div>
                    ))}
                </Carousel>
            </div>
            <h1 className="heading">Meet the Developers!</h1>
            <div className="developers-container col-md-12">
                <div className="row">
                    <div className="dev-container col-md-3">
                        <div className="pic col-md-12">
                            <img
                                src={require("./images/dov.jpg")}
                                alt={"Dev pic"}
                                className="dev-image"
                            ></img>
                        </div>
                        <div className="dev-details col-md-12">
                            <h5 className="dev-name">Dov Zabrowsky</h5>
                            <p className="dev-role">Full-Stack Developer</p>
                        </div>
                    </div>
                    <div className="dev-container col-md-3">
                        <div className="pic col-md-12">
                            <img
                                src={require("./images/sydney.jpg")}
                                alt={"Dev pic"}
                                className="dev-image"
                            ></img>
                        </div>
                        <div className="dev-details col-md-12">
                            <h5 className="dev-name">Sydney Landers</h5>
                            <p className="dev-role">Full-Stack Developer</p>
                        </div>
                    </div>
                    <div className="dev-container col-md-3">
                        <div className="pic col-md-12">
                            <img
                                src={require("./images/Abdallah.jpeg")}
                                alt={"Dev pic"}
                                className="dev-image"
                            ></img>
                        </div>
                        <div className="dev-details col-md-12">
                            <h5 className="dev-name">Abdallah Aboseria</h5>
                            <p className="dev-role">Full-Stack Developer</p>
                        </div>
                    </div>
                    <div className="dev-container col-md-3">
                        <div className="pic col-md-12">
                            <img
                                src={require("./images/kadie.jpg")}
                                alt={"Dev pic"}
                                className="dev-image"
                            ></img>
                        </div>
                        <div className="dev-details col-md-12">
                            <h5 className="dev-name">Kadie Meroney</h5>
                            <p className="dev-role">Full-Stack Developer</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default MainPage;
