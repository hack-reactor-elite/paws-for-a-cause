import "./css/App.css";
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import ProtectedRoute from "./utils/ProtectedRoute.js";
import MainPage from "./MainPage.js";
import Nav from "./Nav.js";
import LoginForm from "./accounts/Login.js";
import SignUpAdopter from "./accounts/SignUpAdopter.js";
import SignUpChoice from "./accounts/SignUpChoice.js";
import SignUpShelter from "./accounts/SignUpShelter.js";
import ShelterApplicationList from "./shelters/ShelterApplicationList.js";
import ShelterApplicationDetail from "./shelters/ShelterApplicationDetail.js";
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import ShelterPetList from "./shelters/ShelterPetList.js";
import CreatePet from "./shelters/ListPetForm.js";
import AdopterListPets from "./adopters/PetsList.js";
import PetDetail from "./adopters/PetDetail";
import ApplicationForm from "./adopters/ApplicationForm.js";
import AdopterApplicationList from "./adopters/AdopterApplicationList.js";

function App() {

    const domain = /https:\/\/[^/]+/;
    const basename = process.env.PUBLIC_URL.replace(domain, '');
    const baseurlName = process.env.REACT_APP_API_HOST

    return (
        <AuthProvider baseUrl={baseurlName}>
            <BrowserRouter basename={basename}>
                <Nav />
                <div className="container">
                    <Routes>
                        <Route path="/" element={<MainPage />} />
                        <Route path="/login" element={<LoginForm />} />
                        <Route path="/signup" element={<SignUpChoice />} />
                        <Route path="/signup/adopter" element={<SignUpAdopter />} />
                        <Route path="/signup/shelter" element={<SignUpShelter />} />
                        <Route path="/shelter/applications" element={<ProtectedRoute><ShelterApplicationList /></ProtectedRoute>} />
                        <Route path="/shelter/applications/:id" element={<ProtectedRoute><ShelterApplicationDetail /></ProtectedRoute>} />
                        <Route path="/shelter/pets/" element={<ProtectedRoute><ShelterPetList /></ProtectedRoute>} />
                        <Route path="/shelter/pets/new" element={<ProtectedRoute><CreatePet /></ProtectedRoute>} />
                        <Route path="/adopter/pets" element={<ProtectedRoute><AdopterListPets /></ProtectedRoute>} />
                        <Route path="/adopter/pets/:pet_id" element={<ProtectedRoute><PetDetail /></ProtectedRoute>} />
                        <Route path="/adopter/applications" element={<ProtectedRoute><AdopterApplicationList /></ProtectedRoute>} />
                        <Route path="/adopter/applications/:pet_id" element={<ProtectedRoute><ApplicationForm /></ProtectedRoute>} />
                    </Routes>
                </div>
            </BrowserRouter>
        </AuthProvider>
    );
}

export default App;
