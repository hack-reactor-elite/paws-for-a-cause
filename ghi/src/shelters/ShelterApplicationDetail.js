import { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import ShelterApplicationList from "./ShelterApplicationList";
import PetDetailCSS from "../css/ApplicationDetail.module.css";

export default function ApplicationDetails(props) {
  const navigate = useNavigate();
  const [processed, setProcessed] = useState(false);
  const [application, setApplication] = useState({});
  const params = useParams();
  const applicationId = params.id;

  const fetchApplicationDetails = async () => {
    const url = `${process.env.REACT_APP_API_HOST}/api/applications/${applicationId}`;
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setApplication(data);
      if (data.status !== "Pending") {
        setProcessed(true);
      }
    }
  };

  useEffect(() => {
    fetchApplicationDetails();
  });

  function handleClick() {
    setProcessed(true);
    navigate(`/shelter/applications`);
  }

  const removePet = async (petId) => {
    const pet_url = `${process.env.REACT_APP_API_HOST}/api/pets/${petId}`;
    const fetchConfig = {
      method: "delete",
    };
    const response = await fetch(pet_url, fetchConfig);
    if (response.ok) {
      ShelterApplicationList();
    }
  };

  const approveApplication = async (appId) => {
    const url = `${process.env.REACT_APP_API_HOST}/api/applications/${appId}`;
    const body = { status: "Approved" };
    const fetchConfig = {
      method: "put",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(body),
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      handleClick();
    }
  };

  const rejectApplication = async (appId) => {
    const url = `${process.env.REACT_APP_API_HOST}/api/applications/${appId}`;
    const body = { status: "Rejected" };
    const fetchConfig = {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(body),
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      setProcessed(true);
      handleClick();
    }
  };
  function handleApprove() {
    approveApplication(applicationId);
    removePet(application.pet?.id);
  }

  return (
    <>
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4 text-center">
            <h1>Review Application</h1>
            <h5>Adoption Application for: {application.pet?.name}</h5>
          </div>
        </div>
        <div className="mt-4 p-4 col-6 border border-secondary rounded-5">
          <h3>Applicant Contact Details:</h3>
          <p>Name: {application.adopter?.account.full_name}</p>
          <p>Email: {application.adopter?.account.email}</p>
        </div>
        <div className="col-3 mt-4 mx-auto rounded-5">
          <img
            className={PetDetailCSS.petImage}
            src={application.pet?.image_url}
            alt="pet"
          ></img>
        </div>
        <div className="offset-3 col-6">
          <div className="mt-4 text-center">
            <h1>Application Answers:</h1>
          </div>
          <div className="mt-4 p-5 border border-secondary rounded-5">
            <h3>Applicant Contact Details:</h3>
            <p>Household Size: {application.adopter?.household_size}</p>
            <p>Household Type: {application.adopter?.household_type}</p>
            <p>
              Number of Children in Household:{" "}
              {application.adopter?.num_of_children}
            </p>
            <p>Other Pets in Household: {application.adopter?.other_pets}</p>
            <p>Pet Allergies: {application.adopter?.pet_allergies}</p>
            <br />
            <p>Veterinarian Listed: {application.adopter?.vet_name}</p>
            <br />
            <p>Additional Info: {application?.additional_information}</p>
          </div>
          <div style={{}}>
            <div style={{ float: "left" }}>
              <button disabled={processed} onClick={() => handleApprove()}>
                Approve
              </button>
            </div>
          </div>
          <div style={{}}>
            <div style={{ float: "right" }}>
              <button
                disabled={processed}
                onClick={() => rejectApplication(applicationId)}
              >
                Reject
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
