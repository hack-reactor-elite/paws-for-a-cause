import React, { useState } from 'react';

function CreatePet() {
    const [name, setName] = useState('');
    const [type, setType] = useState('');
    const [sex, setSex] = useState('');
    const [breed, setBreed] = useState('');
    const [age, setAge] = useState('');
    const [color, setColor] = useState('');
    const [imageUrl, setImageUrl] = useState('');
    const [isFixed, setIsFixed] = useState(false);
    const [isGoodWithChildren, setIsGoodWithChildren] = useState(false);
    const [isGoodWithAnimals, setIsGoodWithAnimals] = useState(false);
    const [description, setDescription] = useState('');
    const username = localStorage.getItem("username")
    const petTypes = ['Cat', 'Dog'];
    const sexOptions = ['Male', 'Female'];

    const [successMessage, setSuccessMessage] = useState('');

    const handleTypeChange = (event) => {
        setType(event.target.value);
    };

    const handleSexChange = (event) => {
        setSex(event.target.value);
    };

    const handleChangeBreed = (event) => {
        setBreed(event.target.value);
    };

    const handleChangeAge = (event) => {
        setAge(event.target.value);
    };

    const handleChangeColor = (event) => {
        setColor(event.target.value);
    };

    const handleChangeImageUrl = (event) => {
        setImageUrl(event.target.value);
    };

    const handleChangeName = (event) => {
        setName(event.target.value);
    };

    const handleChangeFixed = (event) => {
        setIsFixed(event.target.value === 'true');
    };

    const handleChangeGoodWithChildren = (event) => {
        setIsGoodWithChildren(event.target.value === 'true');
    };

    const handleChangeGoodWithAnimals = (event) => {
        setIsGoodWithAnimals(event.target.value === 'true');
    };

    const handleChangeDescription = (event) => {
        setDescription(event.target.value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        const shelterURL = `${process.env.REACT_APP_API_HOST}/api/shelters/${username}`
        const sheltResponse = await fetch(shelterURL)
        let sheltData = ""
        if (sheltResponse.ok) {
            sheltData = await sheltResponse.json()
        } else {
            console.error(sheltResponse)
        }

        const data = {
            name: name,
            type: type,
            sex: sex,
            breed: breed,
            age: age,
            color: color,
            image_url: imageUrl,
            fixed: isFixed,
            good_with_children: isGoodWithChildren,
            good_with_animals: isGoodWithAnimals,
            description: description,
            shelter_id: sheltData.id,
        };

        const petsURL = `${process.env.REACT_APP_API_HOST}/api/pets/`;
        const fetchOptions = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        try {
            const response = await fetch(petsURL, fetchOptions);
            if (response.ok) {
                setName('');
                setType('');
                setSex('');
                setBreed('');
                setAge('');
                setColor('');
                setImageUrl('');
                setIsFixed(false);
                setIsGoodWithChildren(false);
                setIsGoodWithAnimals(false);
                setDescription('');

                setSuccessMessage('Successfully added pet!');

                setTimeout(() => {
                    setSuccessMessage('');
                }, 3500);
            }
        } catch (error) {
            console.error(error);
        }
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Pet</h1>

                    {successMessage && (
                        <div className="alert alert-success">{successMessage}</div>
                    )}

                    <form onSubmit={handleSubmit} id="add-pet-form">
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleChangeName}
                                value={name}
                                placeholder="Name"
                                required
                                name="name"
                                type="text"
                                id="name"
                                className="form-control"
                            />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select
                                onChange={handleTypeChange}
                                value={type}
                                required
                                name="pet_type"
                                id="pet_type"
                                className="form-select"
                            >
                                <option value="" disabled>
                                    Select Pet Type
                                </option>
                                {petTypes.map((typeOption) => (
                                    <option key={typeOption} value={typeOption}>
                                        {typeOption}
                                    </option>
                                ))}
                            </select>
                            <label htmlFor="pet_type">Pet Type</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select
                                onChange={handleSexChange}
                                value={sex}
                                required
                                name="sex"
                                id="sex"
                                className="form-select"
                            >
                                <option value="" disabled>
                                    Select Sex
                                </option>
                                {sexOptions.map((sexOption) => (
                                    <option key={sexOption} value={sexOption}>
                                        {sexOption}
                                    </option>
                                ))}
                            </select>
                            <label htmlFor="sex">Sex</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleChangeBreed}
                                value={breed}
                                placeholder="Breed"
                                required
                                name="breed"
                                type="text"
                                id="breed"
                                className="form-control"
                            />
                            <label htmlFor="breed">Breed</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleChangeAge}
                                value={age}
                                placeholder="Age"
                                required
                                name="age"
                                type="number"
                                id="age"
                                className="form-control"
                            />
                            <label htmlFor="age">Age</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleChangeColor}
                                value={color}
                                placeholder="Color"
                                required
                                name="color"
                                type="text"
                                id="color"
                                className="form-control"
                            />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleChangeImageUrl}
                                value={imageUrl}
                                placeholder="Image URL"
                                required
                                name="image_url"
                                type="url"
                                id="image_url"
                                className="form-control"
                            />
                            <label htmlFor="image_url">Image URL</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select
                                onChange={handleChangeFixed}
                                value={isFixed}
                                required
                                name="fixed"
                                id="fixed"
                                className="form-select"
                            >
                                <option value="" disabled>
                                    Is Fixed?
                                </option>
                                <option value={true}>Yes</option>
                                <option value={false}>No</option>
                            </select>
                            <label htmlFor="fixed">Fixed</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select
                                onChange={handleChangeGoodWithChildren}
                                value={isGoodWithChildren}
                                required
                                name="good_with_children"
                                id="good_with_children"
                                className="form-select"
                            >
                                <option value="" disabled>
                                    Is Good with Children?
                                </option>
                                <option value={true}>Yes</option>
                                <option value={false}>No</option>
                            </select>
                            <label htmlFor="good_with_children">Good with Children</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select
                                onChange={handleChangeGoodWithAnimals}
                                value={isGoodWithAnimals}
                                required
                                name="good_with_animals"
                                id="good_with_animals"
                                className="form-select"
                            >
                                <option value="" disabled>
                                    Is Good with Animals?
                                </option>
                                <option value={true}>Yes</option>
                                <option value={false}>No</option>
                            </select>
                            <label htmlFor="good_with_animals">Good with Animals</label>
                        </div>
                        <div className="form-floating mb-3">
                            <textarea
                                onChange={handleChangeDescription}
                                value={description}
                                placeholder="Description"
                                required
                                name="description"
                                id="description"
                                className="form-control"
                            />
                            <label htmlFor="description">Description</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Add Pet</button>
                    </form>
                    {successMessage && (
                        <div className="alert alert-success mt-3">{successMessage}</div>
                    )}
                </div>
            </div>
        </div>
    );
}

export default CreatePet;
