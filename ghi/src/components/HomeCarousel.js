import React, { useState } from "react";
import Carousel from 'react-bootstrap/Carousel';
import '../css/carousel.css';

const data = [
    {
        image: require('../images/jpg9.jpg'),
        caption: "Save a life, find your next best friend today!",
        description: "Almost 6.3 Million Animals enter animal shelters in the United States every year.",
    },
    {
        image: require('../images/jpg8.jpg'),
        caption: "Want a kitten? Think about adopting an older cat instead!",
        description: "Only 54% of senior cats are adopted on average, compared to 81% of kittens.",
    },
    {
        image: require('../images/jpg7.jpg'),
        caption: "Thinking about getting a dog from a breeder? Think again",
        description: "You can help save one of the 670,000 dogs that are euthanized in shelters annually by adopting through our website instead of buying from breeders."
    }
]

function HomeCarousel() {
    const [index, setIndex] = useState(0);
    const handleSelect = (selectedIndex, e) => {
        setIndex(selectedIndex);
    };

    return (
        <Carousel className="carousel d-flex w-100" activeIndex={index} onSelect={handleSelect}>
            {data.map((slide, i) => {
                return (
                    <Carousel.Item>
                        <img
                            className="d-block w-100"
                            src={slide.image}
                            alt="slider"
                        />
                        <Carousel.Caption>
                            <h3 className="caption">{slide.caption}</h3>
                            <p className="description">{slide.description}</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                )
            })}

        </Carousel>
    );
}
export default HomeCarousel;
