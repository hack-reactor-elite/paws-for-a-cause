import React, { useEffect, useState } from 'react';
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from 'react-router-dom';
import useFetch from "react-fetch-hook"

function SignUpAdopter() {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState([]);
    const [full_name, setFullName] = useState([]);
    const [email, setEmail] = useState([]);
    const [household_type, setHouseholdType] = useState("");
    const [household_size, setHouseholdSize] = useState([]);
    const [num_of_children, setNumChildren] = useState([]);
    const [pet_allergies, setPetAllergies] = useState([]);
    const [other_pets, setOtherPets] = useState([]);
    const [vet_name, setVetName] = useState([]);
    const [vet_address, setVetAddress] = useState([]);
    const [userExists, setUserExists] = useState(false);
    const { login } = useToken();
    const navigate = useNavigate();
    const { data } = useFetch(`${process.env.REACT_APP_API_HOST}/api/accounts/${username}`)
    const houseTypes = [
        { value: "apartment", label: "Apartment" },
        { value: "condo", label: "Condo" },
        { value: "house", label: "House" },
        { value: "other", label: "Other" },
    ]

    const handleUsernameChange = (event) => {
        const value = event.target.value;
        setUsername(value);
    }

    const handlePasswordChange = (event) => {
        const value = event.target.value;
        setPassword(value);
    }

    const handleFullNameChange = (event) => {
        const value = event.target.value;
        setFullName(value);
    }

    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value);
    }
    const handleHouseTypeChange = (event) => {
        const value = event.target.value;
        setHouseholdType(value);
    }
    const handleHouseSizeChange = (event) => {
        const value = event.target.value;
        setHouseholdSize(value);
    }
    const handleNumChildrenChange = (event) => {
        const value = event.target.value;
        setNumChildren(value);
    }
    const handlePetAllergiesChange = (event) => {
        const value = event.target.value;
        setPetAllergies(value);
    }
    const handleOtherPetsChange = (event) => {
        const value = event.target.value;
        setOtherPets(value);
    }
    const handleVetNameChange = (event) => {
        const value = event.target.value;
        setVetName(value);
    }
    const handleVetAddressChange = (event) => {
        const value = event.target.value;
        setVetAddress(value);
    }

    useEffect(() => {
        if (username.trim().length) {
            if (data) {
                setUserExists(true);
            }
            else {
                setUserExists(false);
            }
        }
    }, [username, data])

    const handleSubmit = async (event) => {
        event.preventDefault();
        const accountData = {}
        accountData.username = username;
        accountData.password = password;
        accountData.email = email;
        accountData.full_name = full_name;
        accountData.role = "adopter";

        const adopterData = {}
        adopterData.username = username;
        adopterData.household_type = household_type;
        adopterData.household_size = household_size;
        adopterData.num_of_children = num_of_children;
        adopterData.pet_allergies = pet_allergies;
        adopterData.other_pets = other_pets;
        adopterData.vet_name = vet_name;
        adopterData.vet_address = vet_address;

        const accountsUrl = `${process.env.REACT_APP_API_HOST}/api/accounts`
        const fetchAccountsConfig = {
            method: "post",
            body: JSON.stringify(accountData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const adoptersUrl = `${process.env.REACT_APP_API_HOST}/api/adopters`
        const fetchAdoptersConfig = {
            method: "post",
            body: JSON.stringify(adopterData),
            headers: {
                'Content-Type': 'application/json',
            },
        };


        const accountsResponse = await fetch(accountsUrl, fetchAccountsConfig)
        if (accountsResponse.ok) {
            event.target.reset();
        }
        const adoptersResponse = await fetch(adoptersUrl, fetchAdoptersConfig)
        if (adoptersResponse.ok) {
            event.target.reset();
            localStorage.setItem("username", username)
            login(username, password);
            navigate("/")
        }
    }



    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Enter details to create an account:</h1>
                    {!userExists ? (
                        <div></div>
                    ) : (
                        <div className="alert alert-danger fade show" role="alert">
                            <strong>Account with username already exists.</strong>
                        </div>
                    )}
                    <form onSubmit={handleSubmit} id="create-shelter-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleUsernameChange} value={username} placeholder="username" required type="text" name="username"
                                id="username" className="form-control" />
                            <label htmlFor="name">Username</label>
                            <div className="invalid-feedback" data-sb-feedback="username:required">Username is required.</div>

                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFullNameChange} value={full_name} placeholder="full_name" required type="text" name="full_name"
                                id="vin" className="form-control" />
                            <label htmlFor="style">Full Name</label>

                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePasswordChange} value={password} placeholder="password" type="password" name="password" id="password"
                                className="form-control" required />
                            <label htmlFor="password">Password</label>

                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEmailChange} value={email} placeholder="email" type="email" name="email" id="email"
                                className="form-control" required />
                            <label htmlFor="email">Email</label>

                        </div>
                        <h3>Adoption Application Questions:</h3>
                        <div className="form-floating mb-3">
                            <select onChange={handleHouseTypeChange} value={household_type} className="form-select" id="household_type" aria-label="Household Type" required>
                                <option value="">Select an Option:</option>
                                {houseTypes.map(house => {
                                    return (
                                        <option key={house.value} value={house.value}>{house.label}</option>
                                    )
                                })}
                            </select>
                            <label for="householdType">Household Type</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleHouseSizeChange} className="form-control" value={household_size} id="household_size" type="number" placeholder="Household Size" required />
                            <label for="household_size">Household Size</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleNumChildrenChange} className="form-control" value={num_of_children} id="num_of_children" type="number" placeholder="Number of Children in Household" required />
                            <label for="num_of_children">Number of Children in Household</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePetAllergiesChange} value={pet_allergies} className="form-control" id="pet_allergies" type="text" placeholder="Additional Pets in Household" required />
                            <label for="pet_allergies">List Pet Allergies in the Home:</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleOtherPetsChange} value={other_pets} className="form-control" id="other_pets" type="text" placeholder="Additional Pets in Household" required />
                            <label for="other_pets">Additional Pets in Household</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleVetNameChange} value={vet_name} className="form-control" id="vet_name" type="text" placeholder="Veterinarian Name" data-sb-validations="" required />
                            <label for="vet_name">Veterinarian Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleVetAddressChange} value={vet_address} className="form-control" id="vet_address" type="text" placeholder="Veterinarian Address" data-sb-validations="" required />
                            <label for="vet_address">Veterinarian Address</label>
                        </div>
                        <button className="btn btn-primary">Sign up for Adopter Account!</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SignUpAdopter;
