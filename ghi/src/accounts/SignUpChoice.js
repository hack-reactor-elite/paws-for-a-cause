import { Link } from "react-router-dom";
import '../css/signup.css'

const imgUrl1 = require('../images/jpg12.jpg');
const imgUrl2 = require('../images/jpg11.jpg');

function SignUpChoice() {
    return (
        <div>
            <br></br>
            <h1 className="s-heading">Select a sign up option below:</h1>
            <br></br>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-1">
                    </div>
                    <div className="sign-up col-md-5">
                        <img src={imgUrl1} className="img1" alt="adopter" />
                        <h2>
                            Adopter Sign Up:
                        </h2>
                        <p>
                            As an adopter, you will have access to our database of thousands of animals up for adoption across the country.
                        </p>
                        <p>
                            <Link to="/signup/adopter"><button className="btn btn-primary">
                                Sign up as an Adopter!
                            </button></Link>
                        </p>
                    </div>
                    <div className="sign-up col-md-5">
                        <img src={imgUrl2} className="img1" alt="shelter" />
                        <h2>
                            Shelter Sign Up:
                        </h2>
                        <p>
                            As a shelter, you will be able to list pets available for adoption at your shelter for potential adopters to find.
                        </p>
                        <p>
                            <Link to="/signup/shelter"><button className="btn btn-primary">
                                Sign up as a Shelter!
                            </button></Link>
                        </p>
                    </div>
                    <div className="col-md-1">
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SignUpChoice;
