import useToken from "@galvanize-inc/jwtdown-for-react";
import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";

const LoginForm = () => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const { token, login } = useToken();
    const navigate = useNavigate();
    const [isValid, setIsValid] = useState(true);


    const handleSubmit = async (e) => {
        e.preventDefault();
        const url = `${process.env.REACT_APP_API_HOST}/api/accounts/${username}`;

        try {
            const response = await fetch(url);
            if (response.ok) {
                setIsValid(true);
                localStorage.setItem("username", username)
                login(username, password);
            }
        } catch (e) {
            console.log(e)
            setIsValid(false);
        }

    };

    useEffect(() => {
        if (token) {
            navigate("/")
        }
    })


    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Login</h1>
                        {isValid ? (
                            <div></div>
                        ) : (
                            <div className="alert alert-danger fade show" role="alert">
                                <strong>User Does Not Exist</strong>
                            </div>
                        )}
                        <form onSubmit={(e) => handleSubmit(e)}>
                            <div className="mb-3">
                                <label className="form-label">Username:</label>
                                <input
                                    name="username"
                                    required type="text"
                                    className="form-control"
                                    onChange={(e) => setUsername(e.target.value)}
                                />
                            </div>
                            <div className="mb-3">
                                <label className="form-label">Password:</label>
                                <input
                                    name="password"
                                    required type="password"
                                    className="form-control"
                                    onChange={(e) => setPassword(e.target.value)}
                                />
                            </div>
                            <div>
                                <input className="btn btn-primary" type="submit" value="Login" />
                            </div>
                            <div>
                                <h3>Don't have an account?</h3>
                                <Link to="/signup"><button className="btn btn-primary">
                                    Signup
                                </button></Link>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default LoginForm;
