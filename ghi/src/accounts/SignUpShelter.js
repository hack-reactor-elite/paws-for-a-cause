import React, { useEffect, useState } from 'react';
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from 'react-router-dom';
import useFetch from "react-fetch-hook"

function SignUpShelter() {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState([]);
    const [full_name, setFullName] = useState([]);
    const [email, setEmail] = useState([]);
    const [shelter_name, setShelterName] = useState([]);
    const [address, setAddress] = useState([]);
    const [phone_num, setPhoneNum] = useState([]);
    const [description, setDescription] = useState([]);
    const [userExists, setUserExists] = useState(false);
    const { data } = useFetch(`${process.env.REACT_APP_API_HOST}/api/accounts/${username}`)
    const { login } = useToken();
    const navigate = useNavigate();

    const handleUsernameChange = (event) => {
        const value = event.target.value;
        setUsername(value);
    }

    const handlePasswordChange = (event) => {
        const value = event.target.value;
        setPassword(value);
    }

    const handleFullNameChange = (event) => {
        const value = event.target.value;
        setFullName(value);
    }

    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value);
    }

    const handleShelterNameChange = (event) => {
        const value = event.target.value;
        setShelterName(value);
    }

    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const handleAddressChange = (event) => {
        const value = event.target.value;
        setAddress(value);
    }

    const handlePhoneNumChange = (event) => {
        const value = event.target.value;
        setPhoneNum(value);
    }

    useEffect(() => {
        if (username.trim().length) {
            if (data) {
                setUserExists(true);
            }
            else {
                setUserExists(false);
            }
        }
    }, [username, data])

    const handleSubmit = async (event) => {
        event.preventDefault();
        const accountData = {}
        accountData.username = username;
        accountData.password = password;
        accountData.email = email;
        accountData.full_name = full_name;
        accountData.role = "shelter";

        const shelterData = {}
        shelterData.username = username;
        shelterData.shelter_name = shelter_name;
        shelterData.address = address;
        shelterData.phone_num = phone_num;
        shelterData.description = description;

        const accountsUrl = `${process.env.REACT_APP_API_HOST}/api/accounts`
        const fetchAccountsConfig = {
            method: "post",
            body: JSON.stringify(accountData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const sheltersUrl = `${process.env.REACT_APP_API_HOST}/api/shelters`
        const fetchSheltersConfig = {
            method: "post",
            body: JSON.stringify(shelterData),
            headers: {
                'Content-Type': 'application/json',
            },
        };


        const accountsResponse = await fetch(accountsUrl, fetchAccountsConfig)
        if (accountsResponse.ok) {
            event.target.reset();
        }
        const sheltersResponse = await fetch(sheltersUrl, fetchSheltersConfig)
        if (sheltersResponse.ok) {
            event.target.reset();
            localStorage.setItem("username", username)
            login(username, password);
            navigate("/")
        }
    }






    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Enter details to create an account:</h1>
                    {!userExists ? (
                        <div></div>
                    ) : (
                        <div className="alert alert-danger fade show" role="alert">
                            <strong>Account with username already exists.</strong>
                        </div>
                    )}
                    <form onSubmit={handleSubmit} id="create-shelter-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleUsernameChange} value={username} placeholder="username" required type="text" name="username"
                                id="username" className="form-control" />
                            <label htmlFor="name">Username</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFullNameChange} value={full_name} placeholder="full_name" required type="text" name="full_name"
                                id="vin" className="form-control" />
                            <label htmlFor="style">Full Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePasswordChange} value={password} placeholder="password" required type="password" name="password" id="password"
                                className="form-control" />
                            <label htmlFor="password">Password</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEmailChange} value={email} placeholder="email" required type="email" name="email" id="email"
                                className="form-control" />
                            <label htmlFor="email">Email</label>
                        </div>
                        <h3>Shelter Information:</h3>
                        <div className="form-floating mb-3">
                            <input onChange={handleShelterNameChange} value={shelter_name} placeholder="shelter_name" required type="text" name="shelter_name" id="shelter_name"
                                className="form-control" />
                            <label htmlFor="shelter_name">Shelter Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleAddressChange} value={address} placeholder="address" required type="text" name="address" id="address"
                                className="form-control" />
                            <label htmlFor="address">Shelter Address</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePhoneNumChange} value={phone_num} placeholder="phone_num" required type="tel" pattern="^\d{3}-\d{3}-\d{4}$" name="phone_num" id="phone_num"
                                className="form-control" />
                            <label htmlFor="phone_num">Shelter Phone Number (XXX-XXX-XXXX)</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleDescriptionChange} value={description} placeholder="description" required type="textarea" name="description" id="description"
                                className="form-control" />
                            <label htmlFor="description">Shelter Description</label>
                        </div>
                        <button className="btn btn-primary">Sign up for Shelter Account!</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SignUpShelter;
