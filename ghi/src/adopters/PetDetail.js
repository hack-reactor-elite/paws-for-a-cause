import React, { useState, useEffect } from 'react';
import { useParams, Link } from 'react-router-dom';
import '../css/DetailPet.css';

const PetDetail = () => {
    const { pet_id } = useParams();
    const [pet, setPet] = useState(null);

    useEffect(() => {
        const fetchPetDetails = async () => {
            try {
                const response = await fetch(`${process.env.REACT_APP_API_HOST}/api/pets/${pet_id}`);
                if (response.ok) {
                    const data = await response.json();
                    setPet(data);
                } else {
                    console.error(response);
                }
            } catch (error) {
                console.error(error);
            }
        };

        fetchPetDetails();
    }, [pet_id]);

    return (
        <div className="container-fluid">
            <br></br>
            {pet && (
                <div className="row">
                    <div className="col-md-12">
                        <h2 className="pet-name-text">Meet {pet.name}!</h2>
                        <div className="img-and-detail row">
                            <div className="col-md-7">
                                <div className="img-cont">
                                    <img src={pet.image_url} alt='pet_type' className="pet-image" />
                                </div>
                            </div>
                            <div className="col-md-5">
                                <div className="pet-details-cont">
                                    <div className="pet-details">
                                        <h3 className='details-text'>Details:</h3>
                                        <p>Type: {pet.type}</p>
                                        <p>Breed: {pet.breed}</p>
                                        <p>Sex: {pet.sex}</p>
                                        <p>Color: {pet.color}</p>
                                        <p>Good with Children: {pet.good_with_children ? 'Yes' : 'No'}</p>
                                        <p>Good with Other Pets: {pet.good_with_animals ? 'Yes' : 'No'}</p>
                                        <p>Spayed/Neutered: {pet.fixed ? 'Yes' : 'No'}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="description-container col-md-12">
                                <h3 className="pet-detail-header-3">
                                    About Me!
                                </h3>
                                <div className="row">
                                    <div className="pet-description col-md-12">
                                        <p>{pet.description}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="shelter-details-container col-md-6">
                                <h3 className="pet-detail-header-3">
                                    Where to find me:
                                </h3>
                                <div className="row">
                                    <div className="col-md-12">
                                        {pet.shelter && (
                                            <div className="shelter-details">
                                                <p>Shelter Name: {pet.shelter.shelter_name}</p>
                                                <p>Address: {pet.shelter.address}</p>
                                                <p>Phone Number: {pet.shelter.phone_num}</p>
                                                <p>Shelter Description: {pet.shelter.description}</p>
                                            </div>
                                        )}
                                    </div>
                                </div>
                            </div>
                            <div className="apply-container col-md-6">
                                <h3>
                                    Want to take me home?
                                </h3>
                                <div className="button-cont">
                                    <Link to={`/adopter/applications/${pet_id}`}>
                                        <button className="pet-apply btn btn-primary">Apply to Adopt!</button>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            )}
        </div>

    );
};

export default PetDetail;
