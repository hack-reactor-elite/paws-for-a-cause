import { useEffect, useState } from "react";

function AdopterApplicationList() {
    const username = localStorage.getItem("username");
    const [applications, setApplications] = useState([]);

    const fetchData = async () => {
        const application_url = `${process.env.REACT_APP_API_HOST}/api/applications/adopter/${username}/`;
        const response = await fetch(application_url);
        if (response.ok) {
            const applications = await response.json();
            setApplications(applications);
        }
    };

    useEffect(() => {
        fetchData();
    });

    return (
        <div>
            <h1>My Adoption Applications</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Applicant Name</th>
                        <th>Pet Name</th>
                        <th>Date Submitted</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {applications.map((application) => {
                        return (
                            <tr key={application.id}>
                                <td>{application.adopter.account.full_name}</td>
                                <td>{application.pet.name}</td>
                                <td>{application.date_submitted}</td>
                                <td>{application.status}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
};

export default AdopterApplicationList;
