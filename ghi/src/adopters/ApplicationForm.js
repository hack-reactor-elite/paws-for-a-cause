import { useNavigate } from "react-router-dom";
import { useParams } from 'react-router-dom';


function ApplicationForm() {
    const navigate = useNavigate();
    const username = localStorage.getItem("username");
    const { pet_id } = useParams();

    const handleSubmit = (async event => {
        event.preventDefault();
        const formData = new FormData(event.target);
        const today = new Date();
        const formattedDate = `${String(today.getMonth() + 1).padStart(2, '0')}-${String(today.getDate()).padStart(2, '0')}-${today.getFullYear()}`;
        formData.append("username", username);
        formData.append("pet_id", pet_id);
        formData.append("date_submitted", formattedDate);
        const formJson = JSON.stringify(Object.fromEntries(formData.entries()));
        try {
            const response = await fetch(
                `${process.env.REACT_APP_API_HOST}/api/applications/`,
                { method: "post", body: formJson, headers: { 'Content-Type': 'application/json' } }
            );

            if (response.ok) {
                event.target.reset();
                navigate("/adopter/applications");
            }

        } catch (e) {
            console.error(e);
        }
    });

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Apply to adopt a pet</h1>
                        <form onSubmit={handleSubmit} id="application-form">
                            <div className="form-floating mb-3">
                                <input required placeholder="Additional information" type="text" id="additional_information" name="additional_information" className="form-control" />
                                <label htmlFor="additional_information" className="form-label">Additional information</label>
                            </div>
                            <button type="submit" className="btn btn-primary">Apply</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ApplicationForm;
