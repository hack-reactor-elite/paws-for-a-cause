import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import '../css/ListPets.css';

function AdopterListPets() {
  const [pets, setPets] = useState([]);
  const [applications, setApplications] = useState([]);
  const [age_filter, setAgeFilter] = useState('All');
  const [filter, setFilter] = useState({
    type: '',
    sex: '',
  });

  const handleAgeFilterChange = (event) => {
    const value = event.target.value;
    setAgeFilter(value);
  };

  const isAgeInRange = (petAge, selectedRange) => {
    if (selectedRange === "All") {
      return true;
    }

    const [minAge, maxAge] = selectedRange.split(',').map(Number);
    return petAge >= minAge && petAge < maxAge;
  };

  const fetchPets = async () => {
    try {
      const petResponse = await fetch(`${process.env.REACT_APP_API_HOST}/api/pets`);
      if (petResponse.ok) {
        const petData = await petResponse.json();
        setPets(petData);

        const applicationResponse = await fetch(`${process.env.REACT_APP_API_HOST}/api/applications`);
        if (applicationResponse.ok) {
          const applicationData = await applicationResponse.json();
          setApplications(applicationData);
        }
      } else {
        console.error(petResponse);
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchPets();
  }, []);

  const handleFilterChange = (key, value) => {
    setFilter({ ...filter, [key]: value });
  };

  const getStatusForPet = (pet) => {
    const petApplications = applications.filter(app => app.pet.id === pet.id);
    if (petApplications.length > 0) {
      const mostRecentApplication = petApplications[petApplications.length - 1];
      if (mostRecentApplication.status.toLowerCase() === "pending") {
        return "Available (application pending)";
      } else {
        return "Available";
      }
    } else {
      return "Available";
    }
  };

  const filtered = pets.filter((pet) => {
    const petTypeMatch = !filter.type || pet.type === filter.type;
    const sexMatch = !filter.sex || pet.sex.toLowerCase() === filter.sex.toLowerCase();
    const ageMatch = isAgeInRange(pet.age, age_filter);
    return petTypeMatch && sexMatch && ageMatch;
  });

  return (
    <div>
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <br></br>
            <h1 className="pet-list-heading">Pets Available For Adoption:</h1>
            <form className="filtering" id="filter-form">
              <div className="container-fluid">
                <div className="row">
                  <div className="col-md-12">
                    <div className="row">
                      <div className="col-md-3">
                        <h5 className="filter-head">Filter Results:</h5>
                      </div>
                      <div className="col-md-3 filter-boxes">
                        <div className="form-floating mb-3">
                          <select onChange={(e) => handleFilterChange('type', e.target.value)} value={filter.type} className="form-select" id="type_filter" aria-label="Type Filter">
                            <option value="">Select a Type:</option>
                            <option value="">All</option>
                            <option value="Cat">Cat</option>
                            <option value="Dog">Dog</option>
                          </select>
                          <label for="type_filter">Pet Type</label>
                        </div>
                      </div>
                      <div className="col-md-3 filter-boxes">
                        <div className="form-floating mb-3">
                          <select onChange={(e) => handleFilterChange('sex', e.target.value)} value={filter.sex} className="form-select" id="sex_filter" aria-label="Sex Filter">
                            <option value="">Select a Sex:</option>
                            <option value="">All</option>
                            <option value="Female">Female</option>
                            <option value="Male">Male</option>
                          </select>
                          <label for="sex_filter">Pet Sex</label>
                        </div>
                      </div>
                      <div className="col-md-3 filter-boxes">
                        <div className="form-floating mb-3">
                          <select onChange={handleAgeFilterChange} value={age_filter} className="form-select" id="age_filter" aria-label="Age Filter">
                            <option value="0,100">Select an Age Range:</option>
                            <option value="0,100">All</option>
                            <option value="1,2">1 - 2 Years</option>
                            <option value="2,4">2 - 4 Years</option>
                            <option value="4,6">4 - 6 Years</option>
                            <option value="6,25">6 Years and Older</option>
                          </select>
                          <label for="age_filter">Pet Age</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div className="col-md-12 pet-list-container">
        {filtered.map((pet, index) => (
          <div className="pet-card card" key={index}>
            <div className="card-body">
              <h5 className="pet-name card-text">{pet.name}</h5>
              <img className="pet-card-image"
                src={pet.image_url}
                alt={`${pet.pet_type}`}
              />
              <div className="col-md-12">
                <p className="pet-text-details card-text">Breed: {pet.breed}</p>
                <p className="pet-text-details card-text">Age: {pet.age}</p>
                <p className="pet-text-details card-text">Sex: {pet.sex}</p>
                <p className="pet-text-details card-text">Status: {getStatusForPet(pet)}</p>
              </div>
            </div>
            <div className="card-footer">
              <Link to={`/adopter/pets/${pet.id}`}>
                <button className="btn btn-primary">View Details</button>
              </Link>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default AdopterListPets;
