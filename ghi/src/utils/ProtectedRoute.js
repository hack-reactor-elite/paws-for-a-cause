import useToken from '@galvanize-inc/jwtdown-for-react';
import MainPage from "../MainPage.js";

const ProtectedRoute = ({children}) => {
    const {token} = useToken();

    if (!token) {
        return <MainPage/>;
    }
    return children;
};

export default ProtectedRoute;
