# Paws for a Cause

- Dov Zabrowsky
- Sydney Landers
- Abdallah Aboseria
- Kadie Meroney

Paws for a Cause: Connecting Adopter and Shelters to find animals their forever homes!

## Functionality

Our webiste has two ways for users to engage with our application: as an adopter, or as a shelter.

Any user can:
- Sign up for an account
- Login/Logout
- View our homepage with a carousel of images with related facts about pets and animal shelters.
- View our "featured pets," which are the animals that have been listed on the site the longest.
- View the developers who made the site!

As a shelter, users can:
- List pets at their shelter for adoption
- View a list of pets available at their shelter
- Delete pets listed for adoption in case of in-house adoption
- View a list of adoption applications submitted for pets at their shelter
- View a detailed page of adoption applications submited with information about the prospective adopter
- Approve or reject applications submitted to their shelter.

As an adopter, users can:
- View a list of all adoptable pets on the webiste
- View a detailed view for any pets on the website, which includes more pet information, description, and information about where they are located.
- Apply to adopt a pet through the pet detail pages.
- View a list of submitted applications and their status.

## User Stores/Scenarios

- Given a new user visits the website, when they click on the "Sign Up" button they are given the optino fo selecting user type, then when valid details are entered they become a registered user.
- Given a registered user visits the website, when they click on the "Log In" button and provide their valid login credentials, then they are successfully logged.
- Given a registered user visits the website, when they attempt to log in with incorrect login credentials,then the system displays an error message and the login attempt fails.
- Given a user is logged into their account, when they click on the "Log Out" button, then they are successfully logged out.
- Given a shelter user is logged in, when they list a pet for adoption, then the pet is added to the adoptable pets.
- Given a shelter user is logged in, when they view their shelter's pet listings, then they can see a list of pets available at their shelter.
- Given a shelter user is logged in, when they remove a pet listing, then the pet is removed from the adoptable pets list.
- Given a shelter user is logged in, when they view the adoption applications submitted, then they can see a list of approvable and deniable applications along with applicant information.
- Given a shelter user is logged in, when they approve an adoption application, then the application status is updated to 'Approved' and the pet is removed.
- Given a shelter user is logged in, when they deny an adoption application, then the application status is updated to 'Rejected' and pet application is no longer pending.
- Given an adopter user is logged in, when they visit the available pets, then they can see a list of all adoptable pets from all available shelters for adoption.
- Given an adopter user is checking available pets, when they click on a pet's detials, then they can view detailed information about the pet.
- Given an adopter user is viewing a pet's details, when they decide to submit an application for adoption, then the application is recorded and sent to the shelter with its status set to 'Pending'.
- Given an adopter user is logged in, when they check the status of their "submitted applications", then they can see a list of applications and their statuses.

## Intended market

Our application is an adoption website for people looking to adopt pets into their family AND for shelters looking for a platform to list their animals available for adoption. By connecting shelters and families directly, our application helps to find even more animals their forever homes.

## Stretch Goals

For our project, we had a few stretch goals in mind:
- Adding an interactive map showing where adoptable pets are located
- Ensuring our webiste also works for mobile devices/tablets
- Giving adopters a "list of shelters" view
- Giving adopters a detail view of shelters with a list of their adoptable pets
- The ability to review shelters.
- Adding ability to "favorite" pets you are interested in.
- Adding pets other than cats and dogs.
- Ability to view user profile and update as needed.

## Project Initialization

To fully enjoy this application on your local machine, please make sure to follow these steps:

1. Fork the respository into your own Gitlab repository.
2. Clone the repository down to your local machine
3. CD into the new project directory
4. Run `docker volume create mongo-data`
5. Run `docker compose build`
6. Run `docker compose up`
7. IMPORTANT: In order for the application to work correctly: remove `<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">` from ghi/public/index.html before proceeding with the application.
8. Go to 'http://localhost:3000' to view the frontend applicaiton.
9. The api interface is located at 'http://localhost:8000/docs"

*Note: "Featured Pets" will only show up on the main page once at least 4 pets have been added to the database.

## Tech Stack:
For this project, we used the following tech stack:
- FastAPI
- React
- MongoDB
- Docker
- Bootstrap

## Journaling

Each developer on the team kept an updated developer log of their work throughout the duration of the project at:
- [Journals](https://gitlab.com/hack-reactor-elite/paws-for-a-cause/-/tree/main/journals?ref_type=heads)

## Design

- [API design](docs/apis.md)
- [Data model](docs/data-model.md)
- [GHI](docs/ghi.md)

## Issue Tracking

We used Gitlab Issues to track task and issues for our project. Check out our issues at:
- [Issues](https://gitlab.com/hack-reactor-elite/paws-for-a-cause/-/issues)

## Testing

In this project, we created a total of 16 unit tests to test our backend API endpoints:

- Everyone : [Accounts Unit Tests](https://gitlab.com/hack-reactor-elite/paws-for-a-cause/-/blob/main/api/tests/test_accounts.py?ref_type=heads)
- Sydney Landers: [Adopter Unit Tests](https://gitlab.com/hack-reactor-elite/paws-for-a-cause/-/blob/main/api/tests/test_adopters.py?ref_type=heads)
- Dov Zabrowsky: [Application Unit Tests](https://gitlab.com/hack-reactor-elite/paws-for-a-cause/-/blob/main/api/tests/test_applications.py?ref_type=heads)
- Abdallah Aboseria: [Pet Unit Test](https://gitlab.com/hack-reactor-elite/paws-for-a-cause/-/blob/main/api/tests/test_pets.py?ref_type=heads)
- Kadie Meroney: [Shelter Unit Tests](https://gitlab.com/hack-reactor-elite/paws-for-a-cause/-/blob/main/api/tests/test_shelters.py?ref_type=heads)


