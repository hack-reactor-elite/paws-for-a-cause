from fastapi.testclient import TestClient
from main import app

from queries.accounts import AccountQueries

client = TestClient(app)


class MockAccountQueries:
    def get_all(self):
        return [
            {
                "id": "652da5281eb797763169b8f0",
                "username": "janedoe",
                "email": "janedoe@gmail.com",
                "full_name": "Jane Doe",
                "role": "shelter",
            },
            {
                "id": "652e9449087cd2c5c4b68af3",
                "username": "johndoe",
                "email": "johndoe@gmail.com",
                "full_name": "John Doe",
                "role": "adopter",
            },
        ]

    def get(self, username: str):
        return {
            "id": "652da5281eb797763169b8f0",
            "username": "johndoe",
            "email": "johndoe@gmail.com",
            "full_name": "John Doe",
            "role": "adopter",
        }

    def delete(self, id: str):
        return True


def test_get_all_items():
    app.dependency_overrides[AccountQueries] = MockAccountQueries

    response = client.get("/api/accounts")

    assert response.status_code == 200
    assert len(response.json()) == 2


def test_get_by_username():
    app.dependency_overrides[AccountQueries] = MockAccountQueries

    expected = {
        "id": "652da5281eb797763169b8f0",
        "username": "johndoe",
        "email": "johndoe@gmail.com",
        "full_name": "John Doe",
        "role": "adopter",
    }

    response = client.get("/api/accounts/johndoe")
    assert response.status_code == 200
    assert response.json() == expected


def test_delete():
    app.dependency_overrides[AccountQueries] = MockAccountQueries

    expected = True

    response = client.delete("/api/accounts/652da5281eb797763169b8f0")
    assert response.status_code == 200
    assert response.json() == expected
