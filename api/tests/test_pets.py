from fastapi.testclient import TestClient
from main import app
from queries.pets import PetQueries

client = TestClient(app)


class MockPetQueries:
    def get_pet_by_id(self, pet_id):
        return {
            "id": "652d9b6a47a79d53436a8a96",
            "name": "charlie",
            "type": "dog",
            "sex": "male",
            "breed": "pitbull",
            "age": 1,
            "color": "white",
            "image_url": "www.image.com/image",
            "fixed": True,
            "good_with_children": True,
            "good_with_animals": True,
            "description": "a dog",
            "shelter": {
                "id": "652d9b5c47a79d53436a8a95",
                "shelter_name": "dovs shelter",
                "address": "asfs",
                "phone_num": "364436436",
                "description": "safsagf",
                "account": {
                    "id": "652d9af547a79d53436a8a94",
                    "username": "dovz",
                    "email": "d@d.com",
                    "full_name": "dov zabrowsky",
                    "role": "adopter",
                },
            },
        }

    def get_pets(self):
        return [
            {
                "id": "652d9b6a47a79d53436a8a96",
                "name": "charlie",
                "type": "dog",
                "sex": "male",
                "breed": "pitbull",
                "age": 1,
                "color": "white",
                "image_url": "www.image.com/image",
                "fixed": True,
                "good_with_children": True,
                "good_with_animals": True,
                "description": "a dog",
                "shelter": {
                    "id": "652d9b5c47a79d53436a8a95",
                    "shelter_name": "dovs shelter",
                    "address": "asfs",
                    "phone_num": "364436436",
                    "description": "safsagf",
                    "account": {
                        "id": "652d9af547a79d53436a8a94",
                        "username": "dovz",
                        "email": "d@d.com",
                        "full_name": "dov zabrowsky",
                        "role": "adopter",
                    },
                },
            }
        ]

    def delete_pet(self, pet_id: str):
        return {"message": f"Pet with ID {pet_id} successfully deleted"}


def test_list_pets():
    app.dependency_overrides[PetQueries] = MockPetQueries

    response = client.get("/api/pets/")

    assert response.status_code == 200
    assert len(response.json()) == 1

    app.dependency_overrides = {}


def test_get_pet():
    app.dependency_overrides[PetQueries] = MockPetQueries

    pet_id = "652d9b6a47a79d53436a8a96"
    response = client.get(f"/api/pets/{pet_id}/")

    assert response.status_code == 200

    app.dependency_overrides = {}


def test_delete_pet():
    app.dependency_overrides[PetQueries] = MockPetQueries

    pet_id_to_delete = "652d9b6a47a79d53436a8a96"
    response = client.delete(f"/api/pets/{pet_id_to_delete}")

    assert response.status_code == 200

    app.dependency_overrides = {}
