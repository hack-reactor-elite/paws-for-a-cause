from fastapi.testclient import TestClient
from main import app

from queries.adopters import AdopterQueries
from queries.accounts import AccountQueries

client = TestClient(app)


class MockAccountQueries:
    def get(self, username: str):
        return {
            "id": "652da5281eb797763169b8f0",
            "username": "someone",
            "email": "someone@gmail.com",
            "full_name": "Someone",
            "role": "adopter",
        }


class MockAdopterQueries:
    def get_all(self):
        return [
            {
                "id": "string",
                "household_type": "string",
                "household_size": "string",
                "num_of_children": 0,
                "pet_allergies": "string",
                "other_pets": "string",
                "vet_name": "string",
                "vet_address": "string",
                "account": {
                    "id": "string",
                    "username": "string",
                    "email": "string",
                    "full_name": "string",
                    "role": "string",
                },
            },
            {
                "id": "string",
                "household_type": "string",
                "household_size": "string",
                "num_of_children": 0,
                "pet_allergies": "string",
                "other_pets": "string",
                "vet_name": "string",
                "vet_address": "string",
                "account": {
                    "id": "string",
                    "username": "string",
                    "email": "string",
                    "full_name": "string",
                    "role": "string",
                },
            },
        ]

    def get_by_id(self, username: str):
        return {
            "id": "string",
            "household_type": "string",
            "household_size": "string",
            "num_of_children": 0,
            "pet_allergies": "string",
            "other_pets": "string",
            "vet_name": "string",
            "vet_address": "string",
            "account": {
                "id": "652da5281eb797763169b8f0",
                "username": "someone",
                "email": "someone@gmail.com",
                "full_name": "Someone",
                "role": "adopter",
            },
        }

    def create(self, adopter_in, account_out):
        return {
            "id": "65317c48424ef691a4884b9a",
            "household_type": "house",
            "household_size": "small",
            "num_of_children": 0,
            "pet_allergies": "none",
            "other_pets": "none",
            "vet_name": "none",
            "vet_address": "none",
            "account": {
                "id": "652da5281eb797763169b8f0",
                "username": "user1",
                "email": "user1@gmail.com",
                "full_name": "user 1",
                "role": "adopter",
            },
        }


def test_get_all():
    app.dependency_overrides[AdopterQueries] = MockAdopterQueries

    response = client.get("/api/adopters")
    print(response.json())

    assert response.status_code == 200
    assert len(response.json()) == 2


def test_create():
    app.dependency_overrides[AdopterQueries] = MockAdopterQueries
    app.dependency_overrides[AccountQueries] = MockAccountQueries

    adopter_data = {
        "username": "user1",
        "household_type": "house",
        "household_size": "small",
        "num_of_children": 0,
        "pet_allergies": "none",
        "other_pets": "none",
        "vet_name": "none",
        "vet_address": "none",
    }

    expected = {
        "id": "65317c48424ef691a4884b9a",
        "household_type": "house",
        "household_size": "small",
        "num_of_children": 0,
        "pet_allergies": "none",
        "other_pets": "none",
        "vet_name": "none",
        "vet_address": "none",
        "account": {
            "id": "652da5281eb797763169b8f0",
            "username": "user1",
            "email": "user1@gmail.com",
            "full_name": "user 1",
            "role": "adopter",
        },
    }

    response = client.post("/api/adopters", json=adopter_data)
    assert response.status_code == 200
    assert response.json() == expected
