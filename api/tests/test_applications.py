from fastapi.testclient import TestClient
from main import app
from queries.applications import ApplicationQueries
from queries.adopters import AdopterQueries
from queries.pets import PetQueries
from models.applications import AppStatus, AppIn
from models.adopters import AdopterOut
from models.pets import PetOut

client = TestClient(app)


class MockApplicationQueries:
    def get_all(self):
        return [
            {
                "id": "65319ea2cbfbd7c4dd39012c",
                "status": "Pending",
                "date_submitted": "10-19-2023",
                "additional_information": "I WILL adopt this dog.",
                "adopter": {
                    "id": "65319c2ecbfbd7c4dd390125",
                    "household_type": "house",
                    "household_size": "5",
                    "num_of_children": 3,
                    "pet_allergies": "none",
                    "other_pets": "0",
                    "vet_name": "david",
                    "vet_address": "101 1st street",
                    "account": {
                        "id": "65319c2ecbfbd7c4dd390124",
                        "username": "dov",
                        "email": "dovzab1@gmail.com",
                        "full_name": "dov zabrowsky",
                        "role": "adopter",
                    },
                },
                "pet": {
                    "id": "65319d00cbfbd7c4dd390128",
                    "name": "Charlie",
                    "type": "Dog",
                    "sex": "Male",
                    "breed": "pitbull",
                    "age": 1,
                    "color": "White",
                    "image_url": "https://images.pexels.com/photos"
                    "/11012826/pexels-photo-11012826.jpeg",
                    "fixed": True,
                    "good_with_children": True,
                    "good_with_animals": True,
                    "description": "goo dog",
                    "shelter": {
                        "id": "65319cd8cbfbd7c4dd390127",
                        "shelter_name": "dov's shelter",
                        "address": "1054 1st Street",
                        "phone_num": "732-363-1753",
                        "description": "shelter for pets in need of a home",
                        "account": {
                            "id": "65319cd8cbfbd7c4dd390126",
                            "username": "dovz",
                            "email": "dovzab1@gmail.com",
                            "full_name": "dov zabrowsky",
                            "role": "shelter",
                        },
                    },
                },
            },
            {
                "id": "6531a19dcbfbd7c4dd390132",
                "status": "Pending",
                "date_submitted": "10-19-2023",
                "additional_information": "blah blah blah",
                "adopter": {
                    "id": "6531a0e3cbfbd7c4dd390130",
                    "household_type": "condo",
                    "household_size": "6",
                    "num_of_children": 4,
                    "pet_allergies": "none",
                    "other_pets": "0",
                    "vet_name": "david",
                    "vet_address": "101 1st street",
                    "account": {
                        "id": "6531a0e2cbfbd7c4dd39012f",
                        "username": "davezed",
                        "email": "dovzab1@gmail.com",
                        "full_name": "dov zabrowsky",
                        "role": "adopter",
                    },
                },
                "pet": {
                    "id": "6531a17acbfbd7c4dd390131",
                    "name": "Charlie",
                    "type": "Cat",
                    "sex": "Female",
                    "breed": "pitbull",
                    "age": 5,
                    "color": "Black",
                    "image_url": "https://images.pexels.com/photos"
                    "/11012826/pexels-photo-11012826.jpeg",
                    "fixed": False,
                    "good_with_children": False,
                    "good_with_animals": False,
                    "description": "xfhdgfxg",
                    "shelter": {
                        "id": "6531a0c1cbfbd7c4dd39012e",
                        "shelter_name": "dave's shelter",
                        "address": "1054 1st Street",
                        "phone_num": "732-363-1753",
                        "description": "a shelter for all",
                        "account": {
                            "id": "6531a0c1cbfbd7c4dd39012d",
                            "username": "davezed1",
                            "email": "dovzab1@gmail.com",
                            "full_name": "dov zabrowsky",
                            "role": "shelter",
                        },
                    },
                },
            },
        ]

    def get_by_id(self, id: str):
        db = self.get_all()
        for application in db:
            if application["id"] == id:
                return application
        return None

    def get_all_by_shelter_username(self, username: str):
        applications = []
        db = self.get_all()
        for application in db:
            if (
                application["pet"]["shelter"]["account"]["username"]
                == username
            ):
                applications.append(application)
        return applications

    def get_all_by_adopter_username(self, username: str):
        applications = []
        db = self.get_all()
        for application in db:
            if application["adopter"]["account"]["username"] == username:
                applications.append(application)
        return applications

    def update_application_status(self, id: str, status: AppStatus):
        status = status.dict()
        db = self.get_all()
        result = ""
        for application in db:
            if application["id"] == id:
                result = application
                result["status"] = status["status"]
                return result
        return None

    def create(self, app_in: AppIn, adopter: AdopterOut, pet: PetOut):
        application = {}
        app_in = app_in.dict()
        application["id"] = "6537dcc3bab89015572e3209"
        application["status"] = "Pending"
        application["date_submitted"] = app_in["date_submitted"]
        application["additional_information"] = app_in[
            "additional_information"
        ]
        application["adopter"] = adopter
        application["pet"] = pet
        return application


class MockAdopterQueries:
    def get_by_id(self, username: str):
        if username == "davezed":
            return {
                "id": "6531a0e3cbfbd7c4dd390130",
                "household_type": "condo",
                "household_size": "6",
                "num_of_children": 4,
                "pet_allergies": "none",
                "other_pets": "0",
                "vet_name": "david",
                "vet_address": "101 1st street",
                "account": {
                    "id": "6531a0e2cbfbd7c4dd39012f",
                    "username": "davezed",
                    "email": "dovzab1@gmail.com",
                    "full_name": "dov zabrowsky",
                    "role": "adopter",
                },
            }
        else:
            return None


class MockPetQueries:
    def get_pet_by_id(self, pet_id: str):
        if pet_id == "65319d5acbfbd7c4dd39012a":
            return {
                "id": "65319d5acbfbd7c4dd39012a",
                "name": "carmelia",
                "type": "Cat",
                "sex": "Female",
                "breed": "persian",
                "age": 4,
                "color": "Black",
                "image_url": "https://images.pexels.com/photos"
                "/11012826/pexels-photo-11012826.jpeg",
                "fixed": False,
                "good_with_children": False,
                "good_with_animals": False,
                "description": "a;sjlfhogn",
                "shelter": {
                    "id": "65319cd8cbfbd7c4dd390127",
                    "shelter_name": "dov's shelter",
                    "address": "1054 1st Street",
                    "phone_num": "732-363-1753",
                    "description": "shelter for pets in need of a home",
                    "account": {
                        "id": "65319cd8cbfbd7c4dd390126",
                        "username": "dovz",
                        "email": "dovzab1@gmail.com",
                        "full_name": "dov zabrowsky",
                        "role": "shelter",
                    },
                },
            }
        else:
            return None


def test_list_applications():
    app.dependency_overrides[ApplicationQueries] = MockApplicationQueries

    expected = [
        {
            "id": "65319ea2cbfbd7c4dd39012c",
            "status": "Pending",
            "date_submitted": "10-19-2023",
            "additional_information": "I WILL adopt this dog.",
            "adopter": {
                "id": "65319c2ecbfbd7c4dd390125",
                "household_type": "house",
                "household_size": "5",
                "num_of_children": 3,
                "pet_allergies": "none",
                "other_pets": "0",
                "vet_name": "david",
                "vet_address": "101 1st street",
                "account": {
                    "id": "65319c2ecbfbd7c4dd390124",
                    "username": "dov",
                    "email": "dovzab1@gmail.com",
                    "full_name": "dov zabrowsky",
                    "role": "adopter",
                },
            },
            "pet": {
                "id": "65319d00cbfbd7c4dd390128",
                "name": "Charlie",
                "type": "Dog",
                "sex": "Male",
                "breed": "pitbull",
                "age": 1,
                "color": "White",
                "image_url": "https://images.pexels.com/photos"
                "/11012826/pexels-photo-11012826.jpeg",
                "fixed": True,
                "good_with_children": True,
                "good_with_animals": True,
                "description": "goo dog",
                "shelter": {
                    "id": "65319cd8cbfbd7c4dd390127",
                    "shelter_name": "dov's shelter",
                    "address": "1054 1st Street",
                    "phone_num": "732-363-1753",
                    "description": "shelter for pets in need of a home",
                    "account": {
                        "id": "65319cd8cbfbd7c4dd390126",
                        "username": "dovz",
                        "email": "dovzab1@gmail.com",
                        "full_name": "dov zabrowsky",
                        "role": "shelter",
                    },
                },
            },
        },
        {
            "id": "6531a19dcbfbd7c4dd390132",
            "status": "Pending",
            "date_submitted": "10-19-2023",
            "additional_information": "blah blah blah",
            "adopter": {
                "id": "6531a0e3cbfbd7c4dd390130",
                "household_type": "condo",
                "household_size": "6",
                "num_of_children": 4,
                "pet_allergies": "none",
                "other_pets": "0",
                "vet_name": "david",
                "vet_address": "101 1st street",
                "account": {
                    "id": "6531a0e2cbfbd7c4dd39012f",
                    "username": "davezed",
                    "email": "dovzab1@gmail.com",
                    "full_name": "dov zabrowsky",
                    "role": "adopter",
                },
            },
            "pet": {
                "id": "6531a17acbfbd7c4dd390131",
                "name": "Charlie",
                "type": "Cat",
                "sex": "Female",
                "breed": "pitbull",
                "age": 5,
                "color": "Black",
                "image_url": "https://images.pexels.com/photos"
                "/11012826/pexels-photo-11012826.jpeg",
                "fixed": False,
                "good_with_children": False,
                "good_with_animals": False,
                "description": "xfhdgfxg",
                "shelter": {
                    "id": "6531a0c1cbfbd7c4dd39012e",
                    "shelter_name": "dave's shelter",
                    "address": "1054 1st Street",
                    "phone_num": "732-363-1753",
                    "description": "a shelter for all",
                    "account": {
                        "id": "6531a0c1cbfbd7c4dd39012d",
                        "username": "davezed1",
                        "email": "dovzab1@gmail.com",
                        "full_name": "dov zabrowsky",
                        "role": "shelter",
                    },
                },
            },
        },
    ]

    response = client.get("/api/applications/")
    assert response.status_code == 200
    assert response.json() == expected

    app.dependency_overrides = {}


def test_get_application_by_id():
    app.dependency_overrides[ApplicationQueries] = MockApplicationQueries

    expected = {
        "id": "6531a19dcbfbd7c4dd390132",
        "status": "Pending",
        "date_submitted": "10-19-2023",
        "additional_information": "blah blah blah",
        "adopter": {
            "id": "6531a0e3cbfbd7c4dd390130",
            "household_type": "condo",
            "household_size": "6",
            "num_of_children": 4,
            "pet_allergies": "none",
            "other_pets": "0",
            "vet_name": "david",
            "vet_address": "101 1st street",
            "account": {
                "id": "6531a0e2cbfbd7c4dd39012f",
                "username": "davezed",
                "email": "dovzab1@gmail.com",
                "full_name": "dov zabrowsky",
                "role": "adopter",
            },
        },
        "pet": {
            "id": "6531a17acbfbd7c4dd390131",
            "name": "Charlie",
            "type": "Cat",
            "sex": "Female",
            "breed": "pitbull",
            "age": 5,
            "color": "Black",
            "image_url": "https://images.pexels.com/photos"
            "/11012826/pexels-photo-11012826.jpeg",
            "fixed": False,
            "good_with_children": False,
            "good_with_animals": False,
            "description": "xfhdgfxg",
            "shelter": {
                "id": "6531a0c1cbfbd7c4dd39012e",
                "shelter_name": "dave's shelter",
                "address": "1054 1st Street",
                "phone_num": "732-363-1753",
                "description": "a shelter for all",
                "account": {
                    "id": "6531a0c1cbfbd7c4dd39012d",
                    "username": "davezed1",
                    "email": "dovzab1@gmail.com",
                    "full_name": "dov zabrowsky",
                    "role": "shelter",
                },
            },
        },
    }

    response = client.get("/api/applications/6531a19dcbfbd7c4dd390132/")
    assert response.status_code == 200
    assert response.json() == expected

    app.dependency_overrides = {}


def test_get_applications_by_shelter_username():
    app.dependency_overrides[ApplicationQueries] = MockApplicationQueries

    expected = [
        {
            "id": "65319ea2cbfbd7c4dd39012c",
            "status": "Pending",
            "date_submitted": "10-19-2023",
            "additional_information": "I WILL adopt this dog.",
            "adopter": {
                "id": "65319c2ecbfbd7c4dd390125",
                "household_type": "house",
                "household_size": "5",
                "num_of_children": 3,
                "pet_allergies": "none",
                "other_pets": "0",
                "vet_name": "david",
                "vet_address": "101 1st street",
                "account": {
                    "id": "65319c2ecbfbd7c4dd390124",
                    "username": "dov",
                    "email": "dovzab1@gmail.com",
                    "full_name": "dov zabrowsky",
                    "role": "adopter",
                },
            },
            "pet": {
                "id": "65319d00cbfbd7c4dd390128",
                "name": "Charlie",
                "type": "Dog",
                "sex": "Male",
                "breed": "pitbull",
                "age": 1,
                "color": "White",
                "image_url": "https://images.pexels.com/photos"
                "/11012826/pexels-photo-11012826.jpeg",
                "fixed": True,
                "good_with_children": True,
                "good_with_animals": True,
                "description": "goo dog",
                "shelter": {
                    "id": "65319cd8cbfbd7c4dd390127",
                    "shelter_name": "dov's shelter",
                    "address": "1054 1st Street",
                    "phone_num": "732-363-1753",
                    "description": "shelter for pets in need of a home",
                    "account": {
                        "id": "65319cd8cbfbd7c4dd390126",
                        "username": "dovz",
                        "email": "dovzab1@gmail.com",
                        "full_name": "dov zabrowsky",
                        "role": "shelter",
                    },
                },
            },
        },
    ]

    response = client.get("/api/applications/shelter/dovz/")
    assert response.status_code == 200
    assert response.json() == expected

    app.dependency_overrides = {}


def test_get_applications_by_adopter_username():
    app.dependency_overrides[ApplicationQueries] = MockApplicationQueries

    expected = [
        {
            "id": "65319ea2cbfbd7c4dd39012c",
            "status": "Pending",
            "date_submitted": "10-19-2023",
            "additional_information": "I WILL adopt this dog.",
            "adopter": {
                "id": "65319c2ecbfbd7c4dd390125",
                "household_type": "house",
                "household_size": "5",
                "num_of_children": 3,
                "pet_allergies": "none",
                "other_pets": "0",
                "vet_name": "david",
                "vet_address": "101 1st street",
                "account": {
                    "id": "65319c2ecbfbd7c4dd390124",
                    "username": "dov",
                    "email": "dovzab1@gmail.com",
                    "full_name": "dov zabrowsky",
                    "role": "adopter",
                },
            },
            "pet": {
                "id": "65319d00cbfbd7c4dd390128",
                "name": "Charlie",
                "type": "Dog",
                "sex": "Male",
                "breed": "pitbull",
                "age": 1,
                "color": "White",
                "image_url": "https://images.pexels.com/photos"
                "/11012826/pexels-photo-11012826.jpeg",
                "fixed": True,
                "good_with_children": True,
                "good_with_animals": True,
                "description": "goo dog",
                "shelter": {
                    "id": "65319cd8cbfbd7c4dd390127",
                    "shelter_name": "dov's shelter",
                    "address": "1054 1st Street",
                    "phone_num": "732-363-1753",
                    "description": "shelter for pets in need of a home",
                    "account": {
                        "id": "65319cd8cbfbd7c4dd390126",
                        "username": "dovz",
                        "email": "dovzab1@gmail.com",
                        "full_name": "dov zabrowsky",
                        "role": "shelter",
                    },
                },
            },
        }
    ]

    response = client.get("/api/applications/adopter/dov/")
    assert response.status_code == 200
    assert response.json() == expected

    app.dependency_overrides = {}


def test_update_application_status():
    app.dependency_overrides[ApplicationQueries] = MockApplicationQueries

    expected = {
        "id": "6531a19dcbfbd7c4dd390132",
        "status": "Approved",
        "date_submitted": "10-19-2023",
        "additional_information": "blah blah blah",
        "adopter": {
            "id": "6531a0e3cbfbd7c4dd390130",
            "household_type": "condo",
            "household_size": "6",
            "num_of_children": 4,
            "pet_allergies": "none",
            "other_pets": "0",
            "vet_name": "david",
            "vet_address": "101 1st street",
            "account": {
                "id": "6531a0e2cbfbd7c4dd39012f",
                "username": "davezed",
                "email": "dovzab1@gmail.com",
                "full_name": "dov zabrowsky",
                "role": "adopter",
            },
        },
        "pet": {
            "id": "6531a17acbfbd7c4dd390131",
            "name": "Charlie",
            "type": "Cat",
            "sex": "Female",
            "breed": "pitbull",
            "age": 5,
            "color": "Black",
            "image_url": "https://images.pexels.com/photos"
            "/11012826/pexels-photo-11012826.jpeg",
            "fixed": False,
            "good_with_children": False,
            "good_with_animals": False,
            "description": "xfhdgfxg",
            "shelter": {
                "id": "6531a0c1cbfbd7c4dd39012e",
                "shelter_name": "dave's shelter",
                "address": "1054 1st Street",
                "phone_num": "732-363-1753",
                "description": "a shelter for all",
                "account": {
                    "id": "6531a0c1cbfbd7c4dd39012d",
                    "username": "davezed1",
                    "email": "dovzab1@gmail.com",
                    "full_name": "dov zabrowsky",
                    "role": "shelter",
                },
            },
        },
    }

    status = {"status": "Approved"}

    response = client.put(
        "/api/applications/6531a19dcbfbd7c4dd390132/", json=status
    )
    assert response.status_code == 200
    assert response.json() == expected

    app.dependency_overrides = {}


def test_create_application():
    app.dependency_overrides[ApplicationQueries] = MockApplicationQueries
    app.dependency_overrides[AdopterQueries] = MockAdopterQueries
    app.dependency_overrides[PetQueries] = MockPetQueries

    expected = {
        "id": "6537dcc3bab89015572e3209",
        "status": "Pending",
        "date_submitted": "10-24-2023",
        "additional_information": "can't wait!",
        "adopter": {
            "id": "6531a0e3cbfbd7c4dd390130",
            "household_type": "condo",
            "household_size": "6",
            "num_of_children": 4,
            "pet_allergies": "none",
            "other_pets": "0",
            "vet_name": "david",
            "vet_address": "101 1st street",
            "account": {
                "id": "6531a0e2cbfbd7c4dd39012f",
                "username": "davezed",
                "email": "dovzab1@gmail.com",
                "full_name": "dov zabrowsky",
                "role": "adopter",
            },
        },
        "pet": {
            "id": "65319d5acbfbd7c4dd39012a",
            "name": "carmelia",
            "type": "Cat",
            "sex": "Female",
            "breed": "persian",
            "age": 4,
            "color": "Black",
            "image_url": "https://images.pexels.com/photos"
            "/11012826/pexels-photo-11012826.jpeg",
            "fixed": False,
            "good_with_children": False,
            "good_with_animals": False,
            "description": "a;sjlfhogn",
            "shelter": {
                "id": "65319cd8cbfbd7c4dd390127",
                "shelter_name": "dov's shelter",
                "address": "1054 1st Street",
                "phone_num": "732-363-1753",
                "description": "shelter for pets in need of a home",
                "account": {
                    "id": "65319cd8cbfbd7c4dd390126",
                    "username": "dovz",
                    "email": "dovzab1@gmail.com",
                    "full_name": "dov zabrowsky",
                    "role": "shelter",
                },
            },
        },
    }

    app_in = {
        "username": "davezed",
        "pet_id": "65319d5acbfbd7c4dd39012a",
        "date_submitted": "10-24-2023",
        "additional_information": "can't wait!",
    }

    response = client.post("/api/applications/", json=app_in)
    assert response.status_code == 200
    assert response.json() == expected

    app.dependency_overrides = {}
