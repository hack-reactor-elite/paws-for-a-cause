from fastapi.testclient import TestClient
from main import app

from queries.shelters import ShelterQueries
from queries.accounts import AccountQueries

client = TestClient(app)


class MockAccountQueries:
    def get(self, username: str):
        return {
            "id": "652e9449087cd2c5c4b68af3",
            "username": "johndoe",
            "email": "johndoe@gmail.com",
            "full_name": "John Doe",
            "role": "shelter",
        }


class MockShelterQueries:
    def get_all(self):
        return [
            {
                "id": "652d77d57084ef899beec6cc",
                "shelter_name": "Jane's Shelter",
                "address": "122 Rainbow Road",
                "phone_num": "888-888-8888",
                "description": "Jane's Shelter takes in catsw and dogs.",
                "account": {
                    "id": "652d77d47084ef899beec6cb",
                    "username": "janedoe",
                    "email": "janedoe@gmail.com",
                    "full_name": "Jane Doe",
                    "role": "shelter",
                },
            },
            {
                "id": "652e94db087cd2c5c4b68af4",
                "shelter_name": "John's Shelter",
                "address": "123 Rainbow Road",
                "phone_num": "888-888-8888",
                "description": "John's Shelter takes in cats and dogs.",
                "account": {
                    "id": "652e9449087cd2c5c4b68af3",
                    "username": "johndoe",
                    "email": "johndoe@gmail.com",
                    "full_name": "John Doe",
                    "role": "shelters",
                },
            },
        ]

    def create(self, shelter_in, account_out):
        return {
            "id": "652e94db087cd2c5c4b68af4",
            "shelter_name": "John's Shelter",
            "address": "123 Rainbow Road",
            "phone_num": "888-888-8888",
            "description": "John's Shelter takes in cats and dogs.",
            "account": {
                "id": "652e9449087cd2c5c4b68af3",
                "username": "johndoe",
                "email": "johndoe@gmail.com",
                "full_name": "John Doe",
                "role": "shelter",
            },
        }


def test_get_all():
    app.dependency_overrides[ShelterQueries] = MockShelterQueries

    response = client.get("/api/shelters")
    print(response.json())

    assert response.status_code == 200
    assert len(response.json()) == 2


def test_create():
    app.dependency_overrides[ShelterQueries] = MockShelterQueries
    app.dependency_overrides[AccountQueries] = MockAccountQueries

    shelter_data = {
        "username": "johndoe",
        "shelter_name": "John's Shelter",
        "address": "123 Rainbow Road",
        "phone_num": "888-888-8888",
        "description": "John's Shelter takes in cats and dogs.",
    }

    expected = {
        "id": "652e94db087cd2c5c4b68af4",
        "shelter_name": "John's Shelter",
        "address": "123 Rainbow Road",
        "phone_num": "888-888-8888",
        "description": "John's Shelter takes in cats and dogs.",
        "account": {
            "id": "652e9449087cd2c5c4b68af3",
            "username": "johndoe",
            "email": "johndoe@gmail.com",
            "full_name": "John Doe",
            "role": "shelter",
        },
    }

    response = client.post("/api/shelters", json=shelter_data)
    assert response.status_code == 200
    assert response.json() == expected
