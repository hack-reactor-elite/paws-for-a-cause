from .client import Queries
from .adopters import AdopterOut
from .pets import PetOut
from models.applications import AppIn, AppOut, AppWithExtras, AppStatus
from bson.objectid import ObjectId


class ApplicationQueries(Queries):
    DB_NAME = "paws_db"
    COLLECTION = "applications"

    def get_all(self) -> list[AppOut]:
        db = self.collection.find()
        applications = []
        for app in db:
            app["id"] = str(app["_id"])
            applications.append(AppOut(**app))
        return applications

    def get_by_id(self, id: str) -> AppOut:
        app = self.collection.find_one({"_id": ObjectId(id)})
        app["id"] = str(app["_id"])
        return app

    def get_all_by_shelter_username(self, username: str) -> list[AppOut]:
        db = self.collection.find({"pet.shelter.account.username": username})
        applications = []
        for app in db:
            app["id"] = str(app["_id"])
            applications.append(AppOut(**app))
        return applications

    def get_all_by_adopter_username(self, username: str) -> list[AppOut]:
        db = self.collection.find({"adopter.account.username": username})
        applications = []
        for app in db:
            app["id"] = str(app["_id"])
            applications.append(AppOut(**app))
        return applications

    def update_application_status(
        self, app_id: str, status: AppStatus
    ) -> AppOut | None:
        status = status.dict()
        update_query = {"$set": status}
        result = self.collection.update_one(
            {"_id": ObjectId(app_id)}, update_query
        )
        if result.modified_count > 0:
            app = self.get_by_id(app_id)
            return app
        else:
            return None

    def create(
        self,
        app: AppIn,
        adopter: AdopterOut,
        pet: PetOut,
    ) -> AppWithExtras:
        app = app.dict()
        pet = pet.dict()
        app["adopter"] = adopter
        app["pet"] = pet
        self.collection.insert_one(app)
        app["id"] = str(app["_id"])
        return AppWithExtras(**app)

    def delete(self, id: str) -> bool:
        result = self.collection.delete_one({"_id": ObjectId(id)})
        return result.deleted_count == 1
