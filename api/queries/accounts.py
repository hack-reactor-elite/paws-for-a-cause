from .client import Queries
from models.accounts import Account, AccountIn, AccountOut
from pymongo.errors import DuplicateKeyError
from bson.objectid import ObjectId


class DuplicateAccountError(ValueError):
    pass


class AccountQueries(Queries):
    DB_NAME = "paws_db"
    COLLECTION = "accounts"

    def get(self, username: str) -> Account:
        account = self.collection.find_one({"username": username})
        if not account:
            return None
        account["id"] = str(account["_id"])
        return Account(**account)

    def get_all(self) -> list[AccountOut]:
        db = self.collection.find()
        accounts = []
        for account in db:
            account["id"] = str(account["_id"])
            accounts.append(AccountOut(**account))
        return accounts

    def create(self, info: AccountIn, hashed_password: str) -> Account:
        account = info.dict()
        account["password"] = hashed_password
        try:
            self.collection.insert_one(account)
        except DuplicateKeyError:
            raise DuplicateAccountError()
        account["id"] = str(account["_id"])
        return Account(**account)

    def delete(self, id: str) -> bool:
        return self.collection.delete_one({"_id": ObjectId(id)})
