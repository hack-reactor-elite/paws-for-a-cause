from .client import Queries
from models.adopters import AdopterWithAccount, AdopterIn, AdopterOut
from models.accounts import AccountOut


class AdopterQueries(Queries):
    DB_NAME = "paws_db"
    COLLECTION = "adopters"

    def get_all(self) -> list[AdopterOut]:
        db = self.collection.find()
        adopters = []
        for adopter in db:
            adopter["id"] = str(adopter["_id"])
            adopters.append(AdopterOut(**adopter))
        return adopters

    def get_by_id(self, username: str) -> AdopterOut:
        adopter = self.collection.find_one({"username": username})
        adopter["id"] = str(adopter["_id"])
        return adopter

    def create(
        self,
        info: AdopterIn,
        info2: AccountOut,
    ) -> AdopterWithAccount:
        adopter = info.dict()
        account = info2.dict()
        adopter["account"] = account
        self.collection.insert_one(adopter)
        adopter["id"] = str(adopter["_id"])
        return AdopterWithAccount(**adopter)
