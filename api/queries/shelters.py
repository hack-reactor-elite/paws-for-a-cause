from .client import Queries
from .accounts import AccountOut
from models.shelters import ShelterWithAccount, ShelterIn, ShelterOut
from bson.objectid import ObjectId


class ShelterQueries(Queries):
    DB_NAME = "paws_db"
    COLLECTION = "shelters"

    def get_all(self) -> list[ShelterOut]:
        db = self.collection.find()
        shelters = []
        for shelter in db:
            shelter["id"] = str(shelter["_id"])
            shelters.append(ShelterOut(**shelter))
        return shelters

    def get(self, username: str) -> ShelterOut:
        shelter = self.collection.find_one({"username": username})
        if not shelter:
            return None
        shelter["id"] = str(shelter["_id"])
        return ShelterOut(**shelter)

    def get_by_id(self, id: str) -> ShelterOut:
        shelter = self.collection.find_one({"_id": ObjectId(id)})
        shelter["id"] = str(shelter["_id"])
        return shelter

    def create(
        self,
        info: ShelterIn,
        info2: AccountOut,
    ) -> ShelterWithAccount:
        shelter = info.dict()
        account = info2.dict()
        shelter["account"] = account
        self.collection.insert_one(shelter)
        shelter["id"] = str(shelter["_id"])
        return ShelterWithAccount(**shelter)
