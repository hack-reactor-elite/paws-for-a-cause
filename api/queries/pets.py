from bson.objectid import ObjectId
from .client import Queries
from models.pets import PetIn, PetOut, PetWithExtras
from models.shelters import ShelterOut


class PetQueries(Queries):
    DB_NAME = "paws-db"
    COLLECTION = "pets"

    def get_pet_by_id(self, pet_id: str) -> PetOut:
        pet = self.collection.find_one({"_id": ObjectId(pet_id)})
        if not pet:
            return None
        pet["id"] = str(pet["_id"])
        return PetOut(**pet)

    def get_pets(self) -> list[PetOut]:
        pets = list(self.collection.find())
        if not pets:
            return []
        for pet in pets:
            pet["id"] = str(pet["_id"])
        return [PetOut(**pet) for pet in pets]

    def get_all_by_shelter(self, username: str) -> list[PetOut]:
        db = list(self.collection.find({"shelter.account.username": username}))
        pets = []
        for pet in db:
            pet["id"] = str(pet["_id"])
            pets.append(PetOut(**pet))
        return pets

    def get_featured(self) -> list[PetOut]:
        db = list(self.collection.find())
        pets = []
        if len(db) >= 4:
            for i in range(4):
                pet = db[i]
                pet["id"] = str(pet["_id"])
                pets.append(PetOut(**pet))
        return pets

    def create_pet(self, pet: PetIn, shelter: ShelterOut) -> PetWithExtras:
        pet = pet.dict()
        pet["shelter"] = shelter
        pet_id = self.collection.insert_one(pet).inserted_id
        pet = self.collection.find_one({"_id": pet_id})
        pet["id"] = str(pet["_id"])
        return PetWithExtras(**pet)

    def update_pet(self, pet_id: str, update_fields: dict) -> PetOut:
        update_query = {"$set": update_fields}
        result = self.collection.update_one(
            {"_id": ObjectId(pet_id)}, update_query
        )
        if result.modified_count > 0:
            updated_pet = self.collection.find_one({"_id": ObjectId(pet_id)})
            updated_pet["id"] = str(updated_pet["_id"])
            return PetOut(**updated_pet)
        else:
            return None

    def delete_pet(self, pet_id: str) -> bool:
        result = self.collection.delete_one({"_id": ObjectId(pet_id)})
        return result.deleted_count == 1
