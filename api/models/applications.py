from .adopters import AdopterOut
from .pets import PetOut
from pydantic import BaseModel
from bson.objectid import ObjectId


class PydanticObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value) -> ObjectId:
        if not ObjectId.is_valid(value):
            raise ValueError(f"Not a valid object ID: {value}")
        return value


class AppIn(BaseModel):
    username: str
    pet_id: str
    status: str = "Pending"
    date_submitted: str
    additional_information: str


class AppOut(BaseModel):
    id: str
    status: str
    date_submitted: str
    additional_information: str
    adopter: AdopterOut
    pet: PetOut


class AppStatus(BaseModel):
    status: str


class AppWithExtras(AppIn):
    id: PydanticObjectId
    adopter: AdopterOut
    pet: PetOut
