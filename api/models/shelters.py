from .accounts import AccountOut
from pydantic import BaseModel
from bson.objectid import ObjectId


class PydanticObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value) -> ObjectId:
        if not ObjectId.is_valid(value):
            raise ValueError(f"Not a valid object ID: {value}")
        return value


class ShelterIn(BaseModel):
    username: str
    shelter_name: str
    address: str
    phone_num: str
    description: str


class ShelterOut(BaseModel):
    id: str
    shelter_name: str
    address: str
    phone_num: str
    description: str
    account: AccountOut


class ShelterWithAccount(ShelterIn):
    id: PydanticObjectId
    account: AccountOut
