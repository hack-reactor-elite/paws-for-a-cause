from pydantic import BaseModel
from bson.objectid import ObjectId
from .shelters import ShelterOut


class PydanticObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value) -> ObjectId:
        if not ObjectId.is_valid(value):
            raise ValueError(f"Not a valid object ID: {value}")
        return value


class PetIn(BaseModel):
    name: str
    type: str
    sex: str
    breed: str
    age: int
    color: str
    image_url: str
    fixed: bool
    good_with_children: bool
    good_with_animals: bool
    description: str
    shelter_id: str


class PetOut(BaseModel):
    id: str
    name: str
    type: str
    sex: str
    breed: str
    age: int
    color: str
    image_url: str
    fixed: bool
    good_with_children: bool
    good_with_animals: bool
    description: str
    shelter: ShelterOut


class PetWithExtras(PetIn):
    id: PydanticObjectId
    shelter: ShelterOut
