from .accounts import AccountOut
from pydantic import BaseModel
from bson.objectid import ObjectId


class PydanticObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value) -> ObjectId:
        if not ObjectId.is_valid(value):
            raise ValueError(f"Not a valid object ID: {value}")
        return value


class AdopterIn(BaseModel):
    username: str
    household_type: str
    household_size: str
    num_of_children: int
    pet_allergies: str
    other_pets: str
    vet_name: str
    vet_address: str


class AdopterOut(BaseModel):
    id: str
    household_type: str
    household_size: str
    num_of_children: int
    pet_allergies: str
    other_pets: str
    vet_name: str
    vet_address: str
    account: AccountOut


class AdopterWithAccount(AdopterIn):
    id: PydanticObjectId
    account: AccountOut
