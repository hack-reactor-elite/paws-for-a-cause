from bson.objectid import ObjectId
from pydantic import BaseModel


class PydanticObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value) -> ObjectId:
        if not ObjectId.is_valid(value):
            raise ValueError(f"Not a valid object ID: {value}")
        return value


class AccountIn(BaseModel):
    username: str
    email: str
    password: str
    full_name: str
    role: str


class AccountOut(BaseModel):
    id: str
    username: str
    email: str
    full_name: str
    role: str


class Account(AccountIn):
    id: PydanticObjectId
