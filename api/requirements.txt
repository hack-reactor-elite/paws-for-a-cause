fastapi[all]==0.92.0
jwtdown-fastapi==0.5.0
uvicorn[standard]==0.23.2
pytest
httpx
pymongo==4.5.0
