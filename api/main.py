from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from authenticator import authenticator
import os
from routers import accounts, shelters, pets, adopters, applications

app = FastAPI()
app.include_router(authenticator.router)
app.include_router(accounts.router)
app.include_router(shelters.router)
app.include_router(pets.router)
app.include_router(adopters.router)
app.include_router(applications.router)

origins = [
    os.environ.get("REACT_APP_API_HOST", None),
    os.environ.get("PUBLIC_URL", None),
    os.environ.get("CORS_HOST", None),
    "https://jun-6-et-paws-api.mod3projects.com",
    "https://hack-reactor-elite.gitlab.io/paws-for-a-cause",
    "https://hack-reactor-elite.gitlab.io",
]


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/", tags=["root"])
def root():
    return {"message": "You hit the root path!"}
