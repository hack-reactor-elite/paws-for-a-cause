from fastapi import Depends, APIRouter
from models.applications import AppIn, AppOut, AppStatus
from queries.applications import ApplicationQueries
from queries.adopters import AdopterQueries
from queries.pets import PetQueries

router = APIRouter()


@router.get(
    "/api/applications/", tags=["applications"], response_model=list[AppOut]
)
async def list_applications(applications: ApplicationQueries = Depends()):
    return applications.get_all()


@router.get(
    "/api/applications/{app_id}/", tags=["applications"], response_model=AppOut
)
async def get_application(
    app_id, applications: ApplicationQueries = Depends()
):
    return applications.get_by_id(app_id)


@router.get(
    "/api/applications/shelter/{username}/",
    tags=["applications"],
    response_model=list[AppOut],
)
async def list_applications_by_shelter(
    username: str, applications: ApplicationQueries = Depends()
):
    return applications.get_all_by_shelter_username(username)


@router.get(
    "/api/applications/adopter/{username}/",
    tags=["applications"],
    response_model=list[AppOut],
)
async def list_applications_by_adopter(
    username: str, applications: ApplicationQueries = Depends()
):
    return applications.get_all_by_adopter_username(username)


@router.put(
    "/api/applications/{app_id}/", tags=["applications"], response_model=AppOut
)
async def update_application_status(
    app_id: str,
    status: AppStatus,
    applications: ApplicationQueries = Depends(),
):
    return applications.update_application_status(app_id, status)


@router.post(
    "/api/applications/", tags=["applications"], response_model=AppOut
)
async def create_application(
    app: AppIn,
    applications: ApplicationQueries = Depends(),
    adopters: AdopterQueries = Depends(),
    pets: PetQueries = Depends(),
):
    adopter = adopters.get_by_id(app.username)
    pet = pets.get_pet_by_id(app.pet_id)
    return applications.create(app, adopter, pet)
