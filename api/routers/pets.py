from fastapi import APIRouter, Depends, HTTPException
from models.pets import PetIn, PetOut
from queries.pets import PetQueries
from queries.shelters import ShelterQueries
from bson import ObjectId
from bson.errors import InvalidId

router = APIRouter()


@router.get("/api/pets/", tags=["pets"], response_model=list[PetOut])
async def list_pets(pet_queries: PetQueries = Depends()):
    return pet_queries.get_pets()


@router.get("/api/pets/{pet_id}/", tags=["pets"], response_model=PetOut)
async def get_pet(pet_id: str, pet_queries: PetQueries = Depends()):
    try:
        ObjectId(pet_id)
    except InvalidId:
        raise HTTPException(
            status_code=400,
            detail="Invalid pet_id format. It must be a valid ObjectId.",
        )
    pet = pet_queries.get_pet_by_id(pet_id)
    if pet is None:
        raise HTTPException(status_code=404, detail="Pet not found")
    return pet


@router.get(
    "/api/pets/home/featured/", tags=["pets"], response_model=list[PetOut]
)
async def get_featured(pet_queries: PetQueries = Depends()):
    return pet_queries.get_featured()


@router.get(
    "/api/pets/shelter/{username}/", tags=["pets"], response_model=list[PetOut]
)
async def list_pets_by_shelter(
    username: str,
    pets: PetQueries = Depends(),
):
    return pets.get_all_by_shelter(username)


@router.post("/api/pets/", tags=["pets"], response_model=PetOut)
async def create_pet(
    pet: PetIn,
    pets: PetQueries = Depends(),
    shelters: ShelterQueries = Depends(),
):
    try:
        shelter = shelters.get_by_id(pet.shelter_id)
        return pets.create_pet(pet, shelter)
    except Exception as e:
        raise HTTPException(status_code=422, detail=str(e))


@router.put("/api/pets/{pet_id}/", tags=["pets"], response_model=PetOut)
async def update_pet(
    pet_id: str, pet: PetIn, pet_queries: PetQueries = Depends()
):
    try:
        ObjectId(pet_id)
    except InvalidId:
        raise HTTPException(
            status_code=400,
            detail="Invalid pet_id format. It must be a valid ObjectId.",
        )
    update_fields = pet.dict(exclude_unset=True)
    updated_pet = pet_queries.update_pet(pet_id, update_fields)
    if updated_pet is None:
        raise HTTPException(
            status_code=404, detail="Nonvalid Id or No felids were updated"
        )
    return updated_pet


@router.delete("/api/pets/{pet_id}/", tags=["pets"])
async def delete_pet(pet_id: str, pet_queries: PetQueries = Depends()):
    try:
        ObjectId(pet_id)
    except InvalidId:
        raise HTTPException(
            status_code=400,
            detail="Invalid pet_id format. It must be a valid ObjectId.",
        )
    deletion_result = pet_queries.delete_pet(pet_id)
    if deletion_result:
        return {"message": "Pet deleted successfully"}
    else:
        raise HTTPException(
            status_code=404,
            detail="Failed to delete pet, make sure pet exists",
        )
