from fastapi import Depends, APIRouter
from models.adopters import AdopterIn, AdopterOut
from queries.adopters import AdopterQueries
from queries.accounts import AccountQueries


router = APIRouter()


@router.get(
    "/api/adopters/", tags=["adopters"], response_model=list[AdopterOut]
)
async def list_adopters(adopters: AdopterQueries = Depends()):
    return adopters.get_all()


@router.post("/api/adopters/", tags=["adopters"], response_model=AdopterOut)
async def create_adopter(
    adopter: AdopterIn,
    adopters: AdopterQueries = Depends(),
    accounts: AccountQueries = Depends(),
):
    account = accounts.get(adopter.username)
    return adopters.create(adopter, account)
