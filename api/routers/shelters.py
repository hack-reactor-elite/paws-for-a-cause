from fastapi import Depends, APIRouter
from models.shelters import ShelterIn, ShelterOut
from queries.shelters import ShelterQueries
from queries.accounts import AccountQueries

router = APIRouter()


@router.get(
    "/api/shelters/", tags=["shelters"], response_model=list[ShelterOut]
)
async def list_shelters(shelters: ShelterQueries = Depends()):
    return shelters.get_all()


@router.get(
    "/api/shelters/{username}/", tags=["shelters"], response_model=ShelterOut
)
async def get_shelter(
    username: str,
    shelters: ShelterQueries = Depends(),
):
    return shelters.get(username)


@router.post("/api/shelters/", tags=["shelters"], response_model=ShelterOut)
async def create_shelter(
    shelter: ShelterIn,
    shelters: ShelterQueries = Depends(),
    accounts: AccountQueries = Depends(),
):
    account = accounts.get(shelter.username)
    return shelters.create(shelter, account)
