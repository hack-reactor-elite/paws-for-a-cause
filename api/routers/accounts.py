from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator
from pydantic import BaseModel
from queries.accounts import AccountQueries
from models.accounts import AccountIn, AccountOut


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOut


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.get(
    "/api/accounts/", tags=["accounts"], response_model=list[AccountOut]
)
async def list_accounts(accounts: AccountQueries = Depends()):
    return accounts.get_all()


@router.get(
    "/api/accounts/{username}/", tags=["accounts"], response_model=AccountOut
)
async def get_account(
    username: str,
    accounts: AccountQueries = Depends(),
):
    return accounts.get(username)


@router.post(
    "/api/accounts/",
    tags=["accounts"],
    response_model=AccountToken | HttpError,
)
async def create_account(
    info: AccountIn,
    request: Request,
    response: Response,
    accounts: AccountQueries = Depends(),
):
    hashed_password = authenticator.hash_password(info.password)
    if accounts.get(info.username):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create an account with those credentials",
        )
    else:
        account = accounts.create(info, hashed_password)

    form = AccountForm(
        username=info.username,
        password=info.password,
        email=info.email,
        role=info.role,
    )
    token = await authenticator.login(response, request, form, accounts)
    return AccountToken(account=account, **token.dict())


@router.delete("/api/accounts/{id}/", tags=["accounts"], response_model=bool)
async def delete_account(
    id: str,
    accounts: AccountQueries = Depends(),
):
    return accounts.delete(id)


@router.get(
    "/token", tags=["authentication"], response_model=AccountToken | None
)
async def get_token(
    request: Request,
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> AccountToken | None:
    if account and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }
